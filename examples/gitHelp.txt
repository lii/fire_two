Git kommandon

# Hjälp / hjälp för ett kommando
git help
git add --help

# Se ändringar
git status

# Lägg till filer
git add filnamn.txt

# Ta bort filer
git rm

# Göra commit
git commit -m "skriv ett bra meddelande!"

# Första gången man ladda upp ändringar till bitbucket. 
# efter man kört detta kan man använda git push
git push -u origin master


# Hämta ändringar 
git pull origin master
git pull

# Kontrollera ändringar
git diff

# HEAD
git diff HEAD

# Med ändringar 
git diff --staged
