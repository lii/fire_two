package se.gu.fire.data.inter;

import javax.ejb.Local;
import se.gu.fire.core.StoredFile;

@Local
public interface IStoredFileManager extends IDataAccessO<StoredFile, Long> {

}
