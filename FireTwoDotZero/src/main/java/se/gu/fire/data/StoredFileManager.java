package se.gu.fire.data;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import se.gu.fire.core.StoredFile;
import se.gu.fire.data.inter.IStoredFileManager;
import se.gu.fire.util.FireLogger;

/**
 * Persistence manager for files stored in the database.
 */
@Stateless
@LocalBean
public class StoredFileManager extends AbstractDAO<StoredFile, Long> implements IStoredFileManager {

    public StoredFileManager() {
        super(StoredFile.class);
        FireLogger.logInfo("Created new StoredFileManager");
    }
    
}
