package se.gu.fire.data.inter;

import java.util.List;
import javax.ejb.Local;
import se.gu.fire.core.Assignment;
import se.gu.fire.core.FireUser;
import se.gu.fire.core.StoredFile;
import se.gu.fire.core.StudentGroup;
import se.gu.fire.core.Submission;
import se.gu.fire.core.Submission.SubmissionStatus;

@Local
public interface ISubmissionManager extends IDataAccessO<Submission, Long> {

    List<Submission> getAllByGroup(StudentGroup group);

    List<Submission> getAllByGroup(Assignment assignment, StudentGroup group);

    List<Submission> getAllForGrader(Assignment assignment, FireUser user);

    SubmissionStatus getAssignmentStatus(Assignment assign, StudentGroup group);

    List<StoredFile> getSubmissionFiles(Submission submission);

    boolean isPassed(Assignment assignment, StudentGroup group);

}
