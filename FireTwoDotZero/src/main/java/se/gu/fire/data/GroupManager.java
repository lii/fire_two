package se.gu.fire.data;

import se.gu.fire.data.inter.IGroupManager;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import se.gu.fire.core.FireUser;
import se.gu.fire.core.StudentGroup;
import se.gu.fire.util.FireLogger;

/**
 * Persistence manager for collections of FireUser objects, which share a common
 * group.
 */
@Stateless
@LocalBean
public class GroupManager extends AbstractDAO<StudentGroup, Long> implements IGroupManager {

    public GroupManager() {
        super(StudentGroup.class);
        FireLogger.logInfo("Created new GroupManager");
    }
       
    @Override
    public List<FireUser> getGroupMembers(StudentGroup group) {
        List<FireUser> all = null;
        try {
            String query = "select u.groupMembers from StudentGroup u where u.id = :groupId";
            TypedQuery<FireUser> q = getEntityManager().createQuery(query, FireUser.class);
            q.setParameter("groupId", group.getId());
            q.setMaxResults(100);
            all = q.getResultList();

        } catch (PersistenceException ex) {
            FireLogger.logSevere("Error in getGroupMembers", ex);

        }

        return all;
    }

    /**
     * TODO:
     * In the future return List<StudentGroup> since there could 
     * be more than 1 group.
     * 
     * @param user
     * @return 
     */
    @Override
    public StudentGroup getGroupForUser(FireUser user) {
        List<StudentGroup> all = null;
        try {
            String query = "select g from StudentGroup g where :user MEMBER OF g.groupMembers";
            TypedQuery<StudentGroup> q = getEntityManager().createQuery(query, StudentGroup.class);
            q.setParameter("user", user);
            q.setMaxResults(100);
            all = q.getResultList();

        } catch (PersistenceException ex) {
            FireLogger.logSevere("Error in getGroupForUser", ex);

        }

        if(all.size() > 0) {
            return all.get(0);
        }
        
        return null;
   
    }
}
