package se.gu.fire.data.inter;

import java.util.List;
import javax.persistence.EntityManager;

/**
 * Defines that all DAO must support Create, Read, Update, Delete
 * 
 * @param <T>
 * @param <K> 
 */
public interface IDataAccessO<T, K> {

    public void create(T entity);
    
    public T read(K id);

    public void update(T entity);

    public void delete(T entity);

    public List<T> getAll();

    public List<T> getRange(int firstResult, int maxResults);

    public void setEntityManager(EntityManager em);

    public Class<T> getEntityClass();

    public void clear();
}
