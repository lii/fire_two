package se.gu.fire.data;

import se.gu.fire.data.inter.IDataAccessO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;

import se.gu.fire.util.Consts;
import se.gu.fire.util.FireLogger;

/**
 * Implementation of the CRUD operations used by the Manager classes.
 *
 * This abstract class provides the logic used by the different Manager classes.
 * All methods are implemented however it is abstract to force the use of
 * different managers for all classes. This will enable future overriding of
 * methods, should it become necessary.
 *
 * @param <T> Should be the class to will be used. E g FireUser.
 * @param <K> Should be the Long id.
 */
public abstract class AbstractDAO<T, K> implements IDataAccessO<T, K> {

    private final Class<T> entityClass;

    public AbstractDAO(Class<T> entityClass) {
        this.entityClass = entityClass;

    }
    
    @PersistenceContext(unitName = Consts.PU_NAME)
    private EntityManager entMan;
    
    public EntityManager getEntityManager() {
        return entMan;
    }
    
    /**
     * Used to enable JUnit tests. 
     * @param em 
     */

    @Override
    public void setEntityManager(EntityManager em) {
        this.entMan = em;
    }

    /**
     * Persists the passed object in the database.
     *
     * @param entity The entity to be persisted.
     */
    @Override
    public void create(T entity) {
        FireLogger.logInfo("Persisting entity: {0}", entity);
        getEntityManager().persist(entity);
    }

    /**
     * Deletes the passed object from the database.
     *
     * @param entity The entity to removed
     */
    @Override
    public void delete(T entity) {
        FireLogger.logInfo("Deleting entity: {0}", entity);
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    /**
     * Updates the object passed in the database.
     *
     * @param t Object to be updated.
     */
    @Override
    public void update(T entity) {
        FireLogger.logInfo("Updating entity: {0}", entity);
        getEntityManager().merge(entity);

    }

    /**
     * Tries to read an object with id key in the database.
     *
     * @param id The id of the object that is desired.
     * @return The object or null
     */
    @Override
    public T read(K id) {
        FireLogger.logInfo("Reading entity with id: {0}", id);
        return getEntityManager().find(entityClass, id);

    }

    /**
     * Get all available objects of the same type.
     *
     * @return
     */
    @Override
    public List<T> getAll() {
        return getRange(-1, -1);
    }

    /**
     * Gets up til maxResults objects starting from firstResult
     *
     * @param firstResult
     * @param maxResults
     * @return
     */
    @SuppressWarnings("unchecked")
	@Override
    public List<T> getRange(int firstResult, int maxResults) {
        CriteriaQuery<T> cq = getEntityManager().getCriteriaBuilder().createQuery(entityClass);
        cq.select(cq.from(entityClass));

        Query q = getEntityManager().createQuery(cq);

        if (maxResults > 0) {
            q.setMaxResults(maxResults);

        }

        if (firstResult > 0) {
            q.setFirstResult(firstResult);
            
        }

        return (List<T>) q.getResultList();

    }

    @Override
    public void clear() {
        FireLogger.logInfo("Clearing persistence context");
        try {
            FireLogger.logInfo("Clearing DB");
            getEntityManager().createQuery("DELETE FROM " + entityClass.getSimpleName()).executeUpdate();
            
        } catch (PersistenceException ex) {
            FireLogger.logSevere("Error when querying database: {0}", ex);
        }
    }


    protected List<T> runQuery(String query, Object... params) {
        try {
            TypedQuery<T> q = getEntityManager().createQuery(query, entityClass);
            for (int paramNr = 1; paramNr <= params.length; paramNr++) {
                q.setParameter(paramNr, params[paramNr-1]);
            }
            FireLogger.logInfo("Running query: {0}", q);
            return q.getResultList();
        } catch (PersistenceException ex) {
            FireLogger.logSevere( "Error when querying database: {0}", ex);
        }
        return null;
    }

    @Override
    public Class<T> getEntityClass() {
        return entityClass;
    }

}