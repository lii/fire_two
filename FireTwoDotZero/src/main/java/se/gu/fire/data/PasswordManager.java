package se.gu.fire.data;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.apache.shiro.util.ByteSource;
import se.gu.fire.core.FireUser;
import se.gu.fire.core.Password;
import se.gu.fire.data.inter.IPasswordManager;
import se.gu.fire.util.FireLogger;

/**
 * Persistence manager for Password objects
 */
@Stateless
@LocalBean
public class PasswordManager extends AbstractDAO<Password, Long> implements IPasswordManager {

    public PasswordManager() {
        super(Password.class);
        FireLogger.logInfo("Created new PasswordManager");
    }

    /*
     public PasswordManager(String database) {
     super(Password.class, database);
     }
     * */
    @Override
    public Password findPassword(FireUser user) {

        Password pwdEntity = null;
        try {
            String query =
                    "select p from Password p where p.id = :passId";

            TypedQuery<Password> q = getEntityManager().createQuery(query, Password.class);
            q.setParameter("passId", user.getId());

            pwdEntity = q.getSingleResult();

        }
        catch (NoResultException ex) {
            FireLogger.logInfo("No result ex in findPassword {0}", ex);

        }
        catch (PersistenceException ex) {
            FireLogger.logSevere("Database error {0}", ex);
        }
        return pwdEntity;
    }

    @Override
    public Password findPassword(FireUser user, String password) {
        return findPassword(user.getId(), password);
    }

    @Override
    public Password findPassword(Long userId, String password) throws NoResultException, PersistenceException {

        Password pwdEntity = null;
        try {
            String query =
                    "select p from Password p where p.id = :userId and p.password like :passId";

            TypedQuery<Password> q = getEntityManager().createQuery(query, Password.class);
            q.setParameter(
                    "passId", password);
            q.setParameter(
                    "userId", userId);

            pwdEntity = q.getSingleResult();
        }
        catch (NoResultException ex) {
            // Return null if password not present
        }
        catch (PersistenceException ex) {
            FireLogger.logSevere("Error in isPassed {0}", ex);
        }
        return pwdEntity;
    }

    /**
     * Convenience method for creating a hashed password for user, from a
     * plaintext.
     *
     * @param userID the userID to associate the password with
     * @param plaintextPassword the password in plaintext
     */
    @Override
    public void create(long userID, String plaintextPassword) {

        Password hashedPassword = createHashedPassword(userID, plaintextPassword);

        super.create(hashedPassword);
    }

    /**
     * Convenience method for updating a password.
     *
     * @param userID the ID of the user whose password to apply the update to.
     * This value is interchangeable with the ID of the password itself.
     * @param newPlaintextPassword the new password in plaintext.
     */
    @Override
    public void update(long userID, String newPlaintextPassword) {

        Password updatedPassword = createHashedPassword(userID, newPlaintextPassword);

        super.update(updatedPassword);
    }

    /**
     * Takes a plaintext password and associated userID and produces a hashed
     * Password from it, using the SHA256 scheme.
     *
     * @param userID
     * @param password
     * @return
     */
    public static Password createHashedPassword(long userID, String password) {

        /*
         * Create a salt, and use it to create a hashed password for the user.
         * Finally, store the hashed password and its salt in the database.
         */
        ByteSource salt = new SecureRandomNumberGenerator().nextBytes();
        String hashedPassword = new Sha256Hash(password, salt, 1024).toBase64();

        Password newPassword = new Password(userID, hashedPassword, salt.getBytes());

        return newPassword;
    }
}