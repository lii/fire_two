package se.gu.fire.data;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import se.gu.fire.core.Assignment;
import se.gu.fire.core.StudentGroup;
import se.gu.fire.data.inter.IAssignmentManager;
import se.gu.fire.util.FireLogger;

/**
 * @see AbstractDAO
 *
 */
@Stateless
@LocalBean
public class AssignmentManager extends AbstractDAO<Assignment, Long> implements IAssignmentManager {

    @EJB
    private SubmissionManager subMan;

    public AssignmentManager() {
        super(Assignment.class);
        FireLogger.logInfo("Created new AssignmentManager");
    }

    /**
     * Check if a given group has completed a course
     * 
     * @param group the group to check
     * @return 
     */
    @Override
    public boolean isCompletedCourse(StudentGroup group) {
        for (Assignment assign : getAll()) {
            if (!subMan.isPassed(assign, group)) {
                return false;
            }
        }

        return true;
    }
}