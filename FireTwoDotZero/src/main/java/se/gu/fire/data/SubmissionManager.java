package se.gu.fire.data;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import se.gu.fire.core.Assignment;
import se.gu.fire.core.FireUser;
import se.gu.fire.core.StoredFile;
import se.gu.fire.core.StudentGroup;
import se.gu.fire.core.Submission;
import se.gu.fire.core.Submission.SubmissionStatus;
import se.gu.fire.data.inter.ISubmissionManager;
import se.gu.fire.util.FireLogger;

/**
 * Persistence manager for Submission objects
 */
@Stateless
@LocalBean
public class SubmissionManager extends AbstractDAO<Submission, Long> implements ISubmissionManager {

    public SubmissionManager() {
        super(Submission.class);
        FireLogger.logInfo("Created new SubmissionManager");
    }
   
    @Override
    public boolean isPassed(Assignment assignment, StudentGroup group) {
        Submission all = null;
        try {
            String query =
                     "select s from Submission s where "
                   + "s.submittingGroup.id = :groupId "
                   + "and s.assignment.id = :assignId "
                   + "and s.status like 'Accepted'";

            TypedQuery<Submission> q = getEntityManager().createQuery(query, Submission.class);
            q.setParameter("groupId", group.getId());
            q.setParameter("assignId", assignment.getId());

            all = q.getSingleResult();

        } catch (PersistenceException ex) {
            FireLogger.logSevere("Error in isPassed {0}", ex);

        }

        // If passed it won't be null
        return all != null;
    }

    /**
     * Get the overall status for an assignment for a group, which is the 
     * submission status with the highest level.
     */
    @Override
    public SubmissionStatus getAssignmentStatus(Assignment assign, StudentGroup group) {
        List<Submission> subs = getAllByGroup(assign, group);

        if (subs.isEmpty()) {
            return SubmissionStatus.NotSubmitted;
        }

        return subs.get(0).getSubmissionStatus();

    }

    @Override
    public List<Submission> getAllByGroup(StudentGroup group) {
        List<Submission> all = null;
        try {
            String query = "select s from Submission s where s.submittingGroup.id = :groupId";
            TypedQuery<Submission> q = getEntityManager().createQuery(query, Submission.class);
            q.setParameter("groupId", group.getId());
            q.setMaxResults(100);
            all = q.getResultList();

        } catch (PersistenceException ex) {
            FireLogger.logSevere("Error in getAllByGroup: {0}", ex);
        } 

        return all;

    }

    @Override
    public List<Submission> getAllByGroup(Assignment assignment, StudentGroup group) {
        List<Submission> all = null;
        try {
            String query = "select s from Submission s where "
                    + "s.submittingGroup.id = :groupId and s.assignment.id = :assignId "
                    + "ORDER BY s.submitTime DESC";
            TypedQuery<Submission> q = getEntityManager().createQuery(query, Submission.class);
            q.setParameter("groupId", group.getId());
            q.setParameter("assignId", assignment.getId());
            q.setMaxResults(100);
            all = q.getResultList();

        } catch (PersistenceException ex) {
            FireLogger.logSevere("Error in getAllByGroup {0}", ex);
        } 

        return all;
    }

    @Override
    public List<Submission> getAllForGrader(Assignment assignment, FireUser user) {
        List<Submission> all = null;

        try {
            String query = "select s from Submission s where "
                    + "s.grader.id = :userId and s.assignment.id = :assignId";
            TypedQuery<Submission> q = getEntityManager().createQuery(query, Submission.class);
            q.setParameter("userId", user.getId());
            q.setParameter("assignId", assignment.getId());
            q.setMaxResults(100);
            all = q.getResultList();

        } catch (PersistenceException ex) {
            FireLogger.logSevere("Error in getAllForGrader {0}", ex);

        } 

        return all;
    }
    
    @Override
    public List<StoredFile> getSubmissionFiles(Submission submission) {
        List<StoredFile> all = null;

        try {
            String query = "select s.files from Submission s where s.id = :submissionId";
            TypedQuery<StoredFile> q = getEntityManager().createQuery(query, StoredFile.class);
            q.setParameter("submissionId", submission.getId());
            q.setMaxResults(100);
            all = q.getResultList();

        } catch (PersistenceException ex) {
            FireLogger.logSevere("Error in getSubmissionFiles {0}", ex);

        } 

        return all;
    }
}
