package se.gu.fire.data.inter;

import java.util.List;
import javax.ejb.Local;
import se.gu.fire.core.FireUser;

@Local
public interface IUserManager extends IDataAccessO<FireUser, Long> {

    FireUser findUser(String email);

    List<FireUser> findUsersByFirstName(String firstName);

    List<FireUser> findUsersByLastName(String lastName);

    boolean isAdmin(FireUser user);

    boolean isAdmin(Long userId);

    boolean isGrader(FireUser user);

    boolean isGrader(Long userId);

}
