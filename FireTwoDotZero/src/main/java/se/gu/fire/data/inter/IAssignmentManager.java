package se.gu.fire.data.inter;

import javax.ejb.Local;
import se.gu.fire.core.Assignment;
import se.gu.fire.core.StudentGroup;

@Local
public interface IAssignmentManager extends IDataAccessO<Assignment, Long> {

    boolean isCompletedCourse(StudentGroup group);

}
