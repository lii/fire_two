package se.gu.fire.data.inter;

import java.util.List;
import javax.ejb.Local;
import se.gu.fire.core.FireUser;
import se.gu.fire.core.StudentGroup;

@Local
public interface IGroupManager extends IDataAccessO<StudentGroup, Long> {

    StudentGroup getGroupForUser(FireUser user);

    List<FireUser> getGroupMembers(StudentGroup group);

}
