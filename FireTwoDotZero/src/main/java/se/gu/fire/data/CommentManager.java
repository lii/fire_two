package se.gu.fire.data;

import se.gu.fire.data.inter.ICommentManager;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import se.gu.fire.core.Comment;
import se.gu.fire.util.FireLogger;

/**
 * Persistence manager for Comment objects.
 */
@Stateful
@LocalBean
public class CommentManager extends AbstractDAO<Comment, Long>
    implements ICommentManager {

    public CommentManager() {
        super(Comment.class);
        FireLogger.logInfo("Created new CommentManager");
    }
      
}
