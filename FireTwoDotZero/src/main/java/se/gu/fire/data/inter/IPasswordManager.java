package se.gu.fire.data.inter;

import javax.ejb.Local;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import se.gu.fire.core.FireUser;
import se.gu.fire.core.Password;

@Local
public interface IPasswordManager extends IDataAccessO<Password, Long> {

    void create(long userID, String plaintextPassword);

    Password findPassword(FireUser user);

    Password findPassword(FireUser user, String password);

    Password findPassword(Long userId, String password) throws NoResultException, PersistenceException;

    void update(long userID, String newPlaintextPassword);

}
