package se.gu.fire.data.inter;

import javax.ejb.Local;
import se.gu.fire.core.Comment;

@Local
public interface ICommentManager extends IDataAccessO<Comment, Long> {

}
