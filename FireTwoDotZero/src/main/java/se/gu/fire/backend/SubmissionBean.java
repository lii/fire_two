package se.gu.fire.backend;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import se.gu.fire.core.Assignment;
import se.gu.fire.core.FireUser;
import se.gu.fire.core.StoredFile;
import se.gu.fire.core.StudentGroup;
import se.gu.fire.core.Submission;
import se.gu.fire.data.inter.IAssignmentManager;
import se.gu.fire.data.inter.IGroupManager;
import se.gu.fire.data.inter.ISubmissionManager;
import se.gu.fire.data.inter.IUserManager;
import se.gu.fire.util.FireLogger;
import se.gu.fire.util.FireUtil;

/**
 * Handles the submission page. Uploading of files. Creation of a new
 * Submission.
 */
@ManagedBean(name = "subBean")
@ViewScoped
public class SubmissionBean {


    private Long assignId;

    private Assignment assign;
    private List<Submission> subs;
    private FireUser user;
    private StudentGroup group;

    private String blockReason;

    private List<UploadedFile> uploadedFiles = new ArrayList<>();

    @EJB
    private IAssignmentManager assignMan;

    @EJB
    private ISubmissionManager subMan;

    @EJB
    private IGroupManager groupMan;

    @EJB
    private IUserManager userMan;

    public void viewParamChanged(ValueChangeEvent ev){
        // Sometimes value is string, sometimes long? TODO: Seems like always long?
        long parseLong = Long.parseLong((String)ev.getNewValue().toString());

        FireLogger.logInfo("View param changed: {0} Class: {1}", ev.getNewValue(), ev.getNewValue().getClass());

        assign = assignMan.read(parseLong);
        user = userMan.findUser(FireUtil.getLoggedinUserEmail());
        group = groupMan.getGroupForUser(user);
        subs = subMan.getAllByGroup(assign, group);

        blockReason = FireUtil.calcBlockStatus(assign, subs, new Date());

        FireLogger.logInfo("blockReason: " + blockReason);
    }

    public void handleFileUpload(FileUploadEvent event) {
        FireLogger.logInfo("handleFileUpload: {0}", event.toString());

        for (UploadedFile file : getUploadedFiles()) {
            if (event.getFile().getFileName().equals(file.getFileName())) {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Duplicate filenames not allowed", null));
                return;
            }
        }

        getUploadedFiles().add(event.getFile());
        FacesMessage msg = new FacesMessage(event.getFile().getFileName() + " is uploaded.", null);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }


    public List<UploadedFile> getUploadedFiles() {
        return uploadedFiles;
    }

    public String submit() {

        if (uploadedFiles.isEmpty()) {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Submission not allowed because no files are uploaded.", null));
                return null;
        }

        Submission sub = new Submission.Builder()
                .assignment(assign)
                .submittingGroup(group)
                .build();

        for (UploadedFile file : getUploadedFiles()) {
            sub.addFile(new StoredFile(file.getFileName(),
                    file.getContents(), file.getContentType()));
        }

        FireLogger.logInfo(uploadedFiles.toString());

        subMan.create(sub);

        clearUploadedFiles();

        return "submitConfirmation?faces-redirect=true";
    }

    public void clearUploadedFiles() {
        getUploadedFiles().clear();
    }

    public void deleteFile(UploadedFile file) {
        getUploadedFiles().remove(file);
    }

    public Long getAssignId() {
        return assignId;
    }

    public void setAssignId(Long assignId) {
        this.assignId = assignId;
    }

    public Assignment getAssignment() {
        return assign;
    }

    public String getBlockReason() {
        return blockReason;
    }

    public boolean isSubissionAllowed() {
        return blockReason == null;
    }


}
