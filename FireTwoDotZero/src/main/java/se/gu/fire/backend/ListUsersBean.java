package se.gu.fire.backend;

import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import se.gu.fire.core.FireUser;
import se.gu.fire.data.inter.IUserManager;

@Named("listUsers")
@RequestScoped
public class ListUsersBean {

    @EJB
    private IUserManager userManager;

    public List<FireUser> getAll() {
        return userManager.getAll();
    }
}
