package se.gu.fire.backend;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.mail.MessagingException;
import javax.validation.constraints.Size;
import se.gu.fire.backend.intersession.InterSessionContextBean;
import se.gu.fire.core.FireUser;
import se.gu.fire.mail.Mailer;
import se.gu.fire.backend.constraints.EmailConstraint;
import se.gu.fire.util.FireFaces;
import se.gu.fire.util.FireLogger;
import se.gu.fire.core.UserRole;
import se.gu.fire.data.inter.IUserManager;

/*
 * Backing bean for the registration interface. This bean will collect and
 * validate personal details of a given user, store these details as a 
 * context for the ConfirmRegistrationBean, and send an email to the user
 * with instructions for confirming the registration.
 */
@Named("beginRegistrationBean")
@RequestScoped
public class BeginRegistrationBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
    private IUserManager userManager;
    @Inject
    private InterSessionContextBean interSessionContext;
    /*
     * Settings for the user
     */
    @Size(min = 1, message = "E-mail field is empty")
    @EmailConstraint
    private String email;
    @Size(min = 1, message = "First name field is empty")
    private String fname;
    @Size(min = 1, message = "Last name field is empty")
    private String lname;
    @Size(min = 1, message = "Last name field is empty")
    private String password;
    @Size(min = 10, max = 10, message = "Invalid personal number")
    private String personNummer;

    public BeginRegistrationBean() {
    }

    /*
     * User has entered all credentials, and is sent an email asking for
     * confirmation to log in. We store a temporary session key for this user
     * and use this as an argument to the confirmationpage for validation.
     */
    public void doStartRegistration() {
        
        FireLogger.logInfo("Attempting to start registration process for new user");

        /*
         * Verify that the user does not already exist in the database
         */
        if (userManager.findUser(email) != null) {
            FireFaces.addErrorMessage("The email you entered already exists");
            return;
        }

        /*
         * Create the new FireUser. The actual allocation of an ID
         * and persistence to the database will be done when the user confirms
         * his/her registration
         */
        FireUser user = new FireUser.Builder()
                .firstName(fname)
                .lastName(lname)
                .email(email)
                .personNummer(personNummer)
                .userRole(UserRole.STUDENT)
                .build();

        /*
         * Creat the password and register the user with the 
         * intersessioncontext, and create a sessionkey
         */
        String sessionKey = interSessionContext.addRegistrationSessionData(user, password);
        String confirmationURL = "localhost:8080/PrimeFire/faces/confirmRegistration.xhtml?sessionKey=" + sessionKey;

        /*
         * Used for testing for now. In the future it is the provided email
         * address which will be the recipient
         
         ArrayList<String> recipients = new ArrayList<>();
         recipients.add("firetwodotzero@googlegroups.com");
         recipients.add("christopher.svanefalk@gmail.com");
         */

        String message = new StringBuilder()
                .append("Hello ")
                .append(fname)
                .append(" ")
                .append(lname)
                .append(", \n\n")
                .append("thank you for applying to the Fire system. In order to confirm your registration, please follow the URL below: \n\n")
                .append(confirmationURL)
                .append("\n\nWe hope you will enjoy using fire!\n\nhugs,\n\nthe FireTeam")
                .toString();

        try {
            Mailer.send(email, "Fire: confirm registration", message);
        }
        catch (MessagingException ex) {
            FireLogger.logSevere("Error sending mail for user {0}: {1}", email, ex.getMessage());
            FireFaces.addErrorMessage("Unable to send email", ex.getMessage());
            return;
        }

        FireFaces.addInfoMessage("Registration succesful! Please follow the URL sent to your mail in order to complete the process");
        FireLogger.logInfo("Created registration request {0} and submitted to: {1}", sessionKey, email);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPersonNummer() {
        return personNummer;
    }

    public void setPersonNummer(String personNummer) {
        this.personNummer = personNummer;
    }
}
