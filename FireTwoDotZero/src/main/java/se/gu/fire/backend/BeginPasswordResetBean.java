/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.gu.fire.backend;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.mail.MessagingException;
import se.gu.fire.backend.constraints.EmailConstraint;
import se.gu.fire.backend.intersession.InterSessionContextBean;
import se.gu.fire.core.FireUser;
import se.gu.fire.data.UserManager;
import se.gu.fire.data.inter.IUserManager;
import se.gu.fire.mail.Mailer;
import se.gu.fire.util.FireFaces;
import se.gu.fire.util.FireLogger;

@Named("beginPasswordResetBean")
@RequestScoped
public class BeginPasswordResetBean implements Serializable {
    private static final long serialVersionUID = 1L;

    @Inject
    private InterSessionContextBean interSessionContextBean;
    @EJB
    private IUserManager userManager;
    
    @EmailConstraint
    private String userEmail;
    
    /**
     * Send an email to the user, containing his/her password
     */
    public void beginPasswordReset() {
        FireLogger.logInfo("Attempting to reset password for user: {0}", userEmail);

        if (userEmail == null) {
            FireFaces.addErrorMessage("Email not found in system {0}", userEmail);
        }

        FireUser targetUser = userManager.findUser(userEmail);

        if (targetUser == null) {
            FireLogger.logInfo("Could not find email: {0}", userEmail);
            FireFaces.addWarningMessage("Email not found in system");
            return;
        }

        String sessionKey = interSessionContextBean.addPasswordRecoverySessionData(userEmail);

        StringBuilder toSend = new StringBuilder()
                .append("Hello there ")
                .append(targetUser.getFullName())
                .append("!\n\nPlease follow this url in order to reset your password for the Fire system:\n\nlocalhost:8080/PrimeFire/faces/resetPassword.xhtml?sessionKey=")
                .append(sessionKey)
                .append("\n\nhugs,\n\nthe Fire team");

        try {
            Mailer.send(userEmail, "Fire: reset password", toSend.toString());
        }
        catch (MessagingException ex) {
            FireLogger.logSevere("unable to send mail to user {0}", userEmail);
            FireFaces.addErrorMessage("Unable to send email", ex.getMessage());
            return;
        }

        FireLogger.logInfo("Sent password reset instructions to: {0}", userEmail);
        FireFaces.addInfoMessage("A mail has been sent with instructions on resetting your password");
    }
    
    public InterSessionContextBean getInterSessionContextBean() {
        return interSessionContextBean;
    }

    public void setInterSessionContextBean(InterSessionContextBean interSessionContextBean) {
        this.interSessionContextBean = interSessionContextBean;
    }

    public IUserManager getUserManager() {
        return userManager;
    }

    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }
}
