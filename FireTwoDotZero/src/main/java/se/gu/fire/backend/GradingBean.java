package se.gu.fire.backend;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.primefaces.component.accordionpanel.AccordionPanel;
import org.primefaces.component.menuitem.MenuItem;
import org.primefaces.component.submenu.Submenu;
import org.primefaces.component.tabview.Tab;
import org.primefaces.model.DefaultMenuModel;

import se.gu.fire.core.Assignment;
import se.gu.fire.core.Submission;
import se.gu.fire.data.inter.IAssignmentManager;
import se.gu.fire.data.inter.ISubmissionManager;
import se.gu.fire.util.FireLogger;
import se.gu.fire.util.FireUtil;

@Named("gradingBean")
@RequestScoped
public class GradingBean {

    @EJB
    private IAssignmentManager assignmentManager;
    @EJB
    private ISubmissionManager submissionManager;
    /*
     * The assignmentMenu, listing all assignments for each course currently
     * assisted by this grader
     */
    private final DefaultMenuModel courseView = new DefaultMenuModel();
    private final AccordionPanel assignmentView = new AccordionPanel();

    public GradingBean() {
        FireLogger.logInfo("Created GradingBean for user {0}", FireUtil.getLoggedinUserEmail());
    }

    @PostConstruct
    private void constructBean() {
        //constructAssignmentView();
        constructCourseMenu();
        FireLogger.logInfo("Constructed bean");
    }

    @SuppressWarnings("unused")
	private void constructAssignmentView() {

        Tab tab = new Tab();
        tab.setTitle("Hello");

        assignmentView.getChildren().add(tab);
        assignmentView.setRendered(true);

        FireLogger.logInfo("Constructed assignmentView");
    }

    private void constructCourseMenu() {

        /*
         * For now we default to just one course at a time, since we have not
         * implemented support for multiple courses as of yet.
         */
        Submenu defaultCourse = new Submenu();
        defaultCourse.setLabel("Objekt Orienterad Programmering IT");

        /*
         * add each assignment associated with this course
         */
        ExpressionFactory expressionFactory =
                FacesContext.getCurrentInstance()
                .getApplication()
                .getExpressionFactory();

        for (Assignment assignment : assignmentManager.getAll()) {

            String assignmentTitle = assignment.getTitle();
            MenuItem menuItem = new MenuItem();
            menuItem.setValue(assignmentTitle);

            MethodExpression expression = expressionFactory.createMethodExpression(
                    FacesContext.getCurrentInstance().getELContext(), "#{gradingBean.selectLab('"+ assignmentTitle + "')}", String.class, new Class[]{String.class});

            menuItem.setActionExpression(expression);
            defaultCourse.getChildren().add(menuItem);
        }
        courseView.addSubmenu(defaultCourse);

        FireLogger.logInfo("Constructed courseMenu");
    }

    public String selectLab(String targetLab) {
        FireLogger.logInfo("Moving to lab: {0}", targetLab);
        return "grading?targetLab=" + targetLab;
    }

    public List<Submission> populateAccordion() {

        /*
        for(String key : FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().keySet()) {
            FireLogger.logSevere(key);
        }
        
        
        String targetLab = getTargetLab();
        

        if (targetLab == null || targetLab.isEmpty()) {
            return new ArrayList<Submission>();
        }
        
        List<Submission> submissions = submissionManager.getAll();

        for (Submission submission : submissions) {

            if (!submission.getAssignment().getTitle().equalsIgnoreCase(targetLab)) {
                submissions.remove(submission);
            }
        }
        */
        
        return submissionManager.getAll();
    }

    @SuppressWarnings("unused")
	private String getTargetLab() {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("targetLab");
    }

    public DefaultMenuModel getCourseMenu() {
        return courseView;
    }

    public IAssignmentManager getIAssignmentManager() {
        return assignmentManager;
    }

    public DefaultMenuModel getCourseView() {
        return courseView;
    }

    public AccordionPanel getAssignmentView() {
        return assignmentView;
    }

    public void setIAssignmentManager(IAssignmentManager IAssignmentManager) {
        this.assignmentManager = IAssignmentManager;


    }

    /**
     * Custom menuitem for the purpose of storing associated assignments and
     * information related to them.
     */
    @SuppressWarnings("unused")
	private class AssignmentMenuItem extends MenuItem {

        private static final long serialVersionUID = 1L;
        private Assignment assignment;

        public AssignmentMenuItem(Assignment assignment) {
            super();
            this.assignment = assignment;
            setValue(assignment.getTitle());
        }

        /**
         * Convenience method
         *
         * @param component
         */
        public void addChild(UIComponent component) {
            getChildren().add(component);
        }
        
		public void acceptSubmission(long id) {
        }
    }
}