package se.gu.fire.backend.intersession;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import se.gu.fire.core.FireUser;
import se.gu.fire.util.FireLogger;
import se.gu.fire.util.FireUtil;
import se.gu.fire.util.collections.hashmap.PurgingConcurrentHashMap;

/**
 * Contains data which is meant to persist between sessions without wasting
 * database operations (since the data itself is largely transient).
 */
@Named("intersession")
@ApplicationScoped
public class InterSessionContextBean {

    /*
     * Sessions associated with a registration procedure. Here, we store a
     * HashMap of user details associated with a given session key. This data
     * is used by our registration beans.
     */
    private final PurgingConcurrentHashMap<String, CredentialsTuple> registrationSessionData;
    /*
     * Sessions associated with the password reset process. Here, we simply
     * associate the userID of a given user, together with a session key.
     */
    private final PurgingConcurrentHashMap<String, String> passwordResetSessionData;

    public InterSessionContextBean() {
        registrationSessionData = new PurgingConcurrentHashMap<>();
        passwordResetSessionData = new PurgingConcurrentHashMap<>();
    }

    /**
     * Add a registration context for the current user session
     *
     * @param user the user class for this user
     * @param password the password for this user
     * @return the session key
     */
    public String addRegistrationSessionData(FireUser user, String password) {

        if (registrationSessionData.values().contains(user)) {
            throw new IllegalArgumentException("Registration already pending");
        }

        String sessionKey = FireUtil.generateRandomByteKey(30);

        registrationSessionData.put(sessionKey, new CredentialsTuple(user, password));
        
        FireLogger.logInfo("Created registration session data for user {0} with session key {1}", user, sessionKey);

        return sessionKey;
    }
    
    /**
     * Add a pending password reset session associated with the current users
     * email, and return the key of this session.
     *
     * @param email email of the user
     * @return the session key
     */
    public String addPasswordRecoverySessionData(String email) {

        if (passwordResetSessionData.values().contains(email)) {
            throw new IllegalArgumentException("Password reset already pending");
        }
        String sessionKey = FireUtil.generateRandomByteKey(30);

        passwordResetSessionData.put(sessionKey, email);
        
        FireLogger.logInfo("Created password reset session data for user {0} with session key {1}", email, sessionKey);
        
        return sessionKey;
    }

    public CredentialsTuple getRegistrationSessionData(String key) {

        FireLogger.logInfo("Retrieving registration session data for key {0}", key);

        CredentialsTuple toReturn = registrationSessionData.get(key);

        if (toReturn == null) {
            FireLogger.logWarning("Registration session data for key {0} is null", key);
        }

        return toReturn;
    }

    public String getPasswordResetSessionData(String key) {

        FireLogger.logInfo("Retrieving password reset session data for key {0}", key);

        String toReturn = passwordResetSessionData.get(key);

        if (toReturn == null ) {
            FireLogger.logWarning("Password reset session data for key {0} is null", key);
        }

        return toReturn;

    }
    
}