/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.gu.fire.backend;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import se.gu.fire.backend.intersession.InterSessionContextBean;
import se.gu.fire.core.FireUser;
import se.gu.fire.data.inter.IPasswordManager;
import se.gu.fire.data.inter.IUserManager;
import se.gu.fire.util.FireFaces;
import se.gu.fire.util.FireUtil;

/**
 *
 * @author christopher
 */
@Named("seetingsResetPasswordBean")
@RequestScoped
public class SettingsResetPasswordBean {

    private FireUser currentUser;
    private String firstPassword;
    private String secondPassword;
    @EJB
    private IPasswordManager passwordManager;
    @EJB
    private IUserManager userManager;
    @SuppressWarnings("unused")
	@Inject
    private InterSessionContextBean interSessionContextBean;
    @Inject
    private ResetPasswordBean resetPasswordBean;

    public void doResetPassword() {

        /*
         * Validate passwords
         */
        if (!firstPassword.equals(secondPassword)) {
            FireFaces.addErrorMessage("Passwords do not match");
            return;
        }

        currentUser = getUserFromApplicationContext();
        if (currentUser == null) {
            return;
        }

        resetPasswordBean.doResetPassword(currentUser, firstPassword);
    }

    private FireUser getUserFromApplicationContext() {

        String userEmail = FireUtil.getLoggedinUserEmail();
        if (userEmail == null || userEmail.isEmpty()) {
            FireFaces.addErrorMessage("unable to retrieve an associated user for this session");
            return null;
        }

        FireUser user = userManager.findUser(userEmail);
        if (user == null) {
            FireFaces.addErrorMessage("Error when retrieving user for this session, please contact your administrator");
            return null;
        }

        return user;
    }

    public String getFirstPassword() {
        return firstPassword;
    }

    public void setFirstPassword(String firstPassword) {
        this.firstPassword = firstPassword;
    }

    public String getSecondPassword() {
        return secondPassword;
    }

    public void setSecondPassword(String secondPassword) {
        this.secondPassword = secondPassword;
    }
}
