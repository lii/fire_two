package se.gu.fire.backend;

import com.google.common.base.Strings;
import java.io.ByteArrayInputStream;
import java.util.Collections;
import java.util.Comparator;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import se.gu.fire.core.Assignment;
import se.gu.fire.core.Comment;
import se.gu.fire.core.FireUser;
import se.gu.fire.core.StoredFile;
import se.gu.fire.core.StudentGroup;
import se.gu.fire.core.Submission;
import se.gu.fire.core.Submission.SubmissionStatus;
import se.gu.fire.data.inter.IAssignmentManager;
import se.gu.fire.data.inter.IGroupManager;
import se.gu.fire.data.SubmissionManager;
import se.gu.fire.data.UserManager;
import se.gu.fire.util.FireUtil;

/**
 * Handles the assignment page where users se information about one assignment
 * for one group. Files can be downloaded and comments made.
 */
// NOTE: Using faces managed bean here, to be able to use ViewScope.
@ViewScoped
@ManagedBean(name = "assignBean")
public class AssignmentBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Assignment assign;

    // Map from assign id to value of new comment textbox
    private Map<Long, String> newCommentMap = new LinkedHashMap<>();

    private Long assignId;

    private String activeSubPanels;

    private SubmissionStatus assignStatus;
    private List<Submission> subs;
    private FireUser user;
    private StudentGroup group;

    @EJB
    private IAssignmentManager assignMan;

    @EJB
    private SubmissionManager subMan;

    @EJB
    private IGroupManager groupMan;

    @EJB
    private UserManager userMan;

    public void viewParamChanged(ValueChangeEvent ev) {
        // Sometimes value is string, sometimes long? TODO: Seems like always long?
        long parseLong = Long.parseLong((String)ev.getNewValue().toString());
        
        assign = assignMan.read(parseLong);
        user = userMan.findUser(FireUtil.getLoggedinUserEmail());
        group = groupMan.getGroupForUser(user);
        assignStatus = subMan.getAssignmentStatus(assign, group);
        subs = subMan.getAllByGroup(assign, group);

        Collections.sort(subs, new Comparator<Submission>() {
            @Override public int compare(Submission s1, Submission s2) {
                return s1.getSubmitTime().compareTo(s2.getSubmitTime());
            }
        });

        activeSubPanels = Integer.toString(subs.size() - 1);
    }

    public StreamedContent download(StoredFile file) {
        return new DefaultStreamedContent(
                        new ByteArrayInputStream(file.getFileData()),
                        file.getContentType(),
                        file.getFileName());
    }

    public StreamedContent downloadAll(Submission sub) {
        String filename = sub.getAssignment().getTitle().replace(' ', '_')
                + "_group_" + sub.getSubmittingGroup().getId() + "_all_files.zip";

        return new DefaultStreamedContent(
                        FireUtil.zipFiles(sub.getFiles()),
                        "application/zip", filename);
    }

    public Long getAssignId() {
        return assignId;
    }

    public void setAssignId(Long assignId) {
        this.assignId = assignId;
    }

    public void submitComment(Submission sub) {
        String text = newCommentMap.get(sub.getId());

        if (Strings.isNullOrEmpty(text)) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Empty comments not allowed", null));
            return;
        }

        sub.addComments(new Comment(user, newCommentMap.get(sub.getId())));
        subMan.update(sub);
        getNewCommentMap().remove(sub.getId());
    }

    public SubmissionStatus getAssignmentStatus() {
        return assignStatus;
    }

    public List<Submission> getSubmissions() {
        return  subs;
    }

    public Assignment getAssign() {
        return assign;
    }

    public StudentGroup getGroup() {
        return group;
    }

    public Map<Long, String> getNewCommentMap() {
        return newCommentMap;
    }

    public String getActiveSubPanels() {
        return activeSubPanels;
    }

    public void setActiveSubPanels(String activeSubPanels) {
        this.activeSubPanels = activeSubPanels;
    }
}
