package se.gu.fire.backend;

import java.io.Serializable;
import java.util.Date;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.validation.constraints.Size;
import se.gu.fire.core.Assignment;
import se.gu.fire.data.inter.IAssignmentManager;
import se.gu.fire.util.FireFaces;

@Named("createAssignmentBean")
@RequestScoped
public class CreateAssignmentBean implements Serializable {
    private static final long serialVersionUID = 1L;

    @EJB
    private IAssignmentManager IAssignmentManager;
    @Size(min = 1, message = "Title field is empty")
    private String title;
    @Size(min = 1, message = "Information field is empty")
    private String information;
    private Date finalDeadline;
    private Date firstDeadline;

    public CreateAssignmentBean() {
    }

    public Date getFirstDeadline() {
        return firstDeadline;
    }

    public void setFirstDeadline(Date firstDeadline) {
        this.firstDeadline = firstDeadline;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public Date getFinalDeadline() {
        return finalDeadline;
    }

    public void setFinalDeadline(Date finalDeadline) {
        this.finalDeadline = finalDeadline;
    }

    public void createAssignment() {

        Assignment assignment = new Assignment.Builder()
                .title(title)
                .information(information)
                .finalDeadline(finalDeadline)
                .firstDeadline(firstDeadline)
                .build();
        
        try {
            IAssignmentManager.create(assignment);
            FireFaces.addInfoMessage("New assignment created!");

        } catch (Exception e) {

            Throwable cause = e.getCause();
            if (cause != null && cause instanceof IllegalArgumentException) {
                FireFaces.addInfoMessage(cause.getMessage());
            } else {
                FireFaces.addInfoMessage("Error! Please check log!");

            }
        }
    }
}
