package se.gu.fire.backend;

import java.io.Serializable;
import java.util.HashMap;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import se.gu.fire.util.FireLogger;
import se.gu.fire.util.FireUtil;

/**
 * Provides basic facilities for dynamic navigation across the application
 * interface.
 */
@Named("navigation")
@SessionScoped
public class NavigationBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final HashMap<String, Integer> pages;

    /*
     * An index of pages which are associated with a given tab in the navigation
     * bar. The values correspond to the relative index of the tab associated
     * with a given page.
     */
    static {
        pages = new HashMap<>();

        pages.put("index", 0);
        pages.put("assignmentList", 1);
        pages.put("grading", 2);
        pages.put("groups", 3);
        pages.put("settings", 4);
        pages.put("help", 5);
        pages.put("test", 6);
        pages.put("admin", 7);
    }
    /*
     * The current tab to highlight
     */
    private int tab;

    public int getTab() {
        return tab;
    }

    public void setTab(int tab) {
        this.tab = tab;
    }
    
    public NavigationBean() {
        FireLogger.logInfo("Created NavigationBean for user {0}", FireUtil.getLoggedinUserEmail());
    }

    /**
     * Navigate to a given page. If the is associated with a tab in the
     * navigation menu, make sure the tab is highlighted as the page is loaded.
     *
     * @param to the page to navigate to
     * @return the faces-redirect parameter
     */
    public String navigateTo(String to) {

        /* 
         * This should never fail,
         * but check for Map exceptions anyway.
         */
        if (pages.containsKey(to)) {
            this.tab = pages.get(to);
        }
        return to + "?faces-redirect=true";
    }
}
