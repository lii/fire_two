package se.gu.fire.backend;

import java.util.Date;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import se.gu.fire.core.Assignment;

public class CreateAssignmentListener {
    
    @PreUpdate
    @PrePersist
    private void checkDates(Assignment assignment) {
        
        // Läsligare sen
        Date firstDeadline = assignment.getFirstDeadline();
        Date finalDeadline = assignment.getFinalDeadline();
        
        if (firstDeadline != null) {
            if (finalDeadline == null)  {
                throw new IllegalArgumentException("First deadline selected while final deadline null!");
                    
            } 
            
            if (firstDeadline.after(finalDeadline)) {
                throw new IllegalArgumentException("First deadline is after final deadline!");
                
            }
            
            if (firstDeadline.equals(finalDeadline)) {
                throw new IllegalArgumentException("First and final deadline is same date!");
                
            }

            // TODO: Cant have this check while testing, must create old deadline.
            // Should this check be done somewhere else instead?
//            if(today.after(firstDeadline)) {
//                throw new IllegalArgumentException("First deadline is before todays date!");
//            }
         }
        
        if (finalDeadline == null) {
            throw new IllegalArgumentException("Final deadline must be set!");
        }
         
            // TODO: Cant have this check while testing, must create old deadline.
            // Should this check be done somewhere else instead?
//        if(today.after(finalDeadline)) {
//            throw new IllegalArgumentException("Final deadline is before todays date!");
//        }
    }
}
