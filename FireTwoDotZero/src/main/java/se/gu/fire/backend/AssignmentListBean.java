package se.gu.fire.backend;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import se.gu.fire.core.Assignment;
import se.gu.fire.core.FireUser;
import se.gu.fire.core.StudentGroup;
import se.gu.fire.data.inter.IAssignmentManager;
import se.gu.fire.data.inter.IGroupManager;
import se.gu.fire.data.inter.ISubmissionManager;
import se.gu.fire.data.inter.IUserManager;
import se.gu.fire.util.FireUtil;

/**
 * Displays as list of assignments and the overall status of each. Mostly used
 * to navigate to the assignment page.
 */
@Named(value = "assignListBean")
@SessionScoped
public class AssignmentListBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
    private IAssignmentManager assignMan;
    @EJB
    private ISubmissionManager subMan;
    @EJB
    private IGroupManager groupMan;
    @EJB
    private IUserManager userMan;

    private FireUser activeUser;
    private StudentGroup group;

    public AssignmentListBean() {
    }

    @PostConstruct
    public void init() {
        activeUser = userMan.findUser(FireUtil.getLoggedinUserEmail());
        group = groupMan.getGroupForUser(activeUser);
    }

    public List<Assignment> getAssignments() {
        return assignMan.getAll();
    }

    public String getStatus(String assignIdStr) {
        if (group == null) {
            return "No group";
        }
        return subMan.getAssignmentStatus(assignMan.read(Long.parseLong(assignIdStr)), group).toString();
    }
}
