package se.gu.fire.backend;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import se.gu.fire.core.FireUser;
import se.gu.fire.data.inter.IUserManager;
import se.gu.fire.util.FireUtil;

@RequestScoped
@Named("templateBean")
public class TemplateBean {
    @EJB
    private IUserManager userMan;

    public FireUser getActiveUser() {
        return userMan.findUser(FireUtil.getLoggedinUserEmail());
    }
}
