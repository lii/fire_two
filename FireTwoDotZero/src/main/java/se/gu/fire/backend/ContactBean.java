package se.gu.fire.backend;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.mail.MessagingException;
import se.gu.fire.mail.Mailer;
import se.gu.fire.util.FireFaces;
import se.gu.fire.util.FireLogger;
import se.gu.fire.util.FireUtil;

/**
 * Used for collecting contact information from a client, and sending it back to
 * the developers.
 */
@Named("contactBean")
@RequestScoped
public class ContactBean {

    private String message = "";

    public ContactBean() {
        FireLogger.logInfo("Created ContactBean for user {0}", FireUtil.getLoggedinUserEmail());
    }

    public String doSend() {

        if (message.isEmpty()) {
            FireFaces.addErrorMessage("Your message is empty!");
            return "";
        }

        String email = FireUtil.getLoggedinUserEmail();

        StringBuilder toSend = new StringBuilder();
        toSend.append("You have received feedback from Fire.\n\n")
                .append("From user: ")
                .append(FireUtil.getLoggedinUserEmail())
                .append("\n\nFeedback: ")
                .append(message);
        
        try {
            Mailer.send(email, "Fire: new feedback received", toSend.toString());
        }
        catch (MessagingException ex) {
            FireLogger.logSevere("unable to send mail to user {0}", email);
            FireFaces.addErrorMessage("Unable to send email", ex.getMessage());
            return "";
        }
        FireLogger.logInfo("User feedback from {0} has been sent!", email);
        return "index?faces-redirect=true";
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
