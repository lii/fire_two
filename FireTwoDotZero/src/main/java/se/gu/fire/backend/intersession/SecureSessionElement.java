package se.gu.fire.backend.intersession;

import se.gu.fire.util.collections.hashmap.AbstractSecureElement;

/**
 * Element to be stored as either key or value in a Secure HashMap.
 */
public class SecureSessionElement<E> extends AbstractSecureElement {

    private final E element;

    public SecureSessionElement(E element, String checksum) {
        super(checksum);

        this.element = element;
    }

    public E getElement() {
        return element;
    }
}
