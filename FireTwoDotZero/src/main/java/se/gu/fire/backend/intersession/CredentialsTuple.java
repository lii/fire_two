package se.gu.fire.backend.intersession;

import se.gu.fire.core.FireUser;

/**
 * Used for intersession transfer of users and associated passwords
 */
public class CredentialsTuple {

    /*
     * Will be refactored when we migrate password...this is le ugly
     */
    private final FireUser user;
    private final String password;

    public FireUser getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public CredentialsTuple(FireUser user, String password) {
        this.user = user;
        this.password = password;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }        
        
        final CredentialsTuple other = (CredentialsTuple) obj;
        return this.user.equals(other.user);
    }
}