package se.gu.fire.backend;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import org.apache.shiro.SecurityUtils;
import se.gu.fire.util.FireLogger;
import se.gu.fire.util.FireUtil;

/**
 * Backing bean handling logout requests
 * @author christopher
 */
@Named("logoutBean")
@RequestScoped
public class LogoutBean {
    
    public LogoutBean(){
        FireLogger.logInfo("Created LogoutBean for user {0}", FireUtil.getLoggedinUserEmail());
    }
    
    /**
     * Log the current user out, destroying his/her session context.
     */
    public String doLogout() {
        SecurityUtils.getSubject().logout();

        return "loggedOut?faces-redirect=true";
    }
}
