/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.gu.fire.backend;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import se.gu.fire.core.FireUser;
import se.gu.fire.data.inter.IUserManager;
import se.gu.fire.util.FireLogger;
import se.gu.fire.util.FireUtil;

@Named("settingsBean")
@RequestScoped
public class SettingsBean {
    
    private FireUser user;
    
    @EJB
    private IUserManager userManager;
    
    public SettingsBean() {
        FireLogger.logInfo("Creating new UserSettingsBean for user {0}", FireUtil.getLoggedinUserEmail());
    }
    
    @PostConstruct
    public void fetchUser() {
        user = userManager.findUser(FireUtil.getLoggedinUserEmail());
    }
    
    public void performUpdate() {
        userManager.update(user);
    }
    
    public String getEmail() {
        return user.getEmail();
    }

    public void setEmail(String email) {
        user.setEmail(email);
    }

    public String getFname() {
        return user.getFname();
    }

    public void setFname(String fname) {
        user.setFname(fname);
    }

    public String getLname() {
        return user.getLname();
    }

    public void setLname(String lname) {
        user.setLname(lname);
    }
    
    public String getFullName() {
        return user.getFullName();
    }
    
    public String getPersonalNumber() {
        return user.getPersonnummer();
    }

    /*
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    */
}
