package se.gu.fire.backend;

import javax.ejb.EJB;
import javax.inject.Named;
import javax.mail.MessagingException;
import javax.persistence.PersistenceException;
import se.gu.fire.core.FireUser;
import se.gu.fire.core.Password;
import se.gu.fire.data.inter.IPasswordManager;
import se.gu.fire.data.inter.IUserManager;
import se.gu.fire.mail.Mailer;
import se.gu.fire.util.FireFaces;
import se.gu.fire.util.FireLogger;
import se.gu.fire.util.FireUtil;

/**
 * Provides functionality associated with resetting a users password
 */
@Named("resetPasswordBean")
public class ResetPasswordBean {

    @EJB
    private IPasswordManager passwordManager;
    @EJB
    private IUserManager userManager;

    public ResetPasswordBean() {
        FireLogger.logInfo("Created ResetPasswordBean for user {0}", FireUtil.getLoggedinUserEmail());
    }
    
    /**
     * Reset the password for a given user
     * 
     * @param user the user whose password to reset
     * @param plainTextPassword plaintext of the new password
     * @return the new password
     */
    public Password doResetPassword(FireUser user, String plainTextPassword) {
        
        try {
            passwordManager.update(user.getId(), plainTextPassword);
        }
        catch (PersistenceException pe) {
            FireFaces.addErrorMessage("Database trouble", "please try again");
            return null;
        }

        /*
         * Send confirmation mail to user
         */
        String toSend = new StringBuilder()
                .append("Hello there ")
                .append(user.getFullName())
                .append("!\n\nYour shiny new password for Fire is: ")
                .append(plainTextPassword)
                .append("\n\nhugs,\n\nthe Fire team")
                .toString();

        try {
            Mailer.send(user.getEmail(), "Fire: your new password", toSend);
        }
        catch (MessagingException ex) {
            FireLogger.logSevere("unable to send mail to user {0}", user.getEmail());
            FireFaces.addErrorMessage("We really wanted to send you and email, but we failed :(");
        }

        return passwordManager.findPassword(user);
    }
}
