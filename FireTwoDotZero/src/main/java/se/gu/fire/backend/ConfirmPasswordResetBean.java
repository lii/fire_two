package se.gu.fire.backend;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import se.gu.fire.backend.intersession.InterSessionContextBean;
import se.gu.fire.core.FireUser;
import se.gu.fire.core.Password;
import se.gu.fire.data.inter.IUserManager;
import se.gu.fire.data.PasswordManager;
import se.gu.fire.util.FireFaces;
import se.gu.fire.util.FireLogger;

/**
 *
 * @author christopher
 */
@Named("confirmPasswordResetBean")
@RequestScoped
public class ConfirmPasswordResetBean {

    private FireUser currentUser;
    private String firstPassword;
    private String secondPassword;
    @EJB

    private PasswordManager passwordManager;
    @EJB
    private IUserManager userManager;
    @Inject
    private InterSessionContextBean interSessionContextBean;
    @Inject
    private ResetPasswordBean resetPasswordBean;

    public String doResetPassword() {

        /*
         * Validate passwords
         */
        if (!firstPassword.equals(secondPassword)) {
            FireFaces.addErrorMessage("Passwords do not match");
            return "";
        }

        currentUser = getUserFromInterSessionContext();
        if (currentUser == null) {
            return "";
        }

        Password password = resetPasswordBean.doResetPassword(currentUser, firstPassword);
        
        if(password == null) {
            FireFaces.addErrorMessage("Unable to reset password");
            return "";
        }
        
        FireFaces.addInfoMessage("Your new password has been emailed to you");
        return "login?faces-redirect=true";
    }

    private FireUser getUserFromInterSessionContext() {

        String sessionKey = getSessionKey();
        if (sessionKey == null || sessionKey.isEmpty()) {
            FireFaces.addErrorMessage("Invalid sessionkey");
            return null;
        }

        String userEmail = interSessionContextBean.getPasswordResetSessionData(sessionKey);
        if (userEmail == null || userEmail.isEmpty()) {
            FireFaces.addErrorMessage("unable to retrieve an associated user for this session");
            return null;
        }

        FireUser user = userManager.findUser(userEmail);
        if (user == null) {
            FireFaces.addErrorMessage("Error when retrieving user for this session, please contact your administrator");
            return null;
        }

        return user;
    }

    /**
     * Retrieve the validation number from the HTTP request
     */
    private String getSessionKey() {

        FireLogger.logInfo("Retrieving sessionKey");

        return FacesContext
                .getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("sessionKey");
    }
    
    public String getFirstPassword() {
        return firstPassword;
    }

    public void setFirstPassword(String firstPassword) {
        this.firstPassword = firstPassword;
    }

    public String getSecondPassword() {
        return secondPassword;
    }
    
        public void setSecondPassword(String secondPassword) {
        this.secondPassword = secondPassword;
    }
}
