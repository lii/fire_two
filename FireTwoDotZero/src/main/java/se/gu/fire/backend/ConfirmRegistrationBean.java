package se.gu.fire.backend;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import se.gu.fire.backend.intersession.CredentialsTuple;
import se.gu.fire.backend.intersession.InterSessionContextBean;
import se.gu.fire.core.FireUser;
import se.gu.fire.data.PasswordManager;
import se.gu.fire.data.UserManager;
import se.gu.fire.data.inter.IPasswordManager;
import se.gu.fire.data.inter.IUserManager;
import se.gu.fire.util.FireFaces;
import se.gu.fire.util.FireLogger;

/**
 * Handles incoming confirm-registration calls, and writes users to the database
 *
 */
@Named("confirmRegistrationBean")
@RequestScoped
public class ConfirmRegistrationBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @EJB
    private IUserManager userManager;
    @EJB
    private IPasswordManager passwordManager;

    @Inject
    private InterSessionContextBean interSessionContext;
    
    public ConfirmRegistrationBean() {
        FireLogger.logInfo("Created new ConfirmRegistrationBean");
    }

    public void doFinishRegistration() {

        String sessionKey = getSessionKey();
        FireLogger.logInfo("Completing registration for seesion key: {0}", sessionKey);

        CredentialsTuple credentials = interSessionContext.getRegistrationSessionData(sessionKey);

        if (credentials == null) {
            FireFaces.addErrorMessage("Error during registration process", "session key does not exist, please try registering again" );
            return;
//            return "An error has occured, please try registering again";
        }

        /*
         * Persist the user
         */
        FireUser newUser = credentials.getUser();
        String password = credentials.getPassword();

        userManager.create(newUser);
        newUser = userManager.findUser(newUser.getEmail());

        /*
         * Persist the password
         */
        passwordManager.create(newUser.getId(), password);

        /*
         * Finally, if everything went well, notify user of success.
         */
        FireLogger.logInfo("Succesfully registered new user: {0}", newUser);
        FireFaces.addInfoMessage(new StringBuilder()
                .append("Welcome ")
                .append(newUser.getFullName())
                .append("!\n\n")
                .append("We hope you will enjoy using Fire2!")
                .toString());

        return;
//        return "apa";
    }

    /**
     * Retrieve the validation number from the HTTP request
     */
    private String getSessionKey() {

        FireLogger.logInfo("Retrieving sessionKey");
        
        return FacesContext
                .getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("sessionKey");
    }

    public IUserManager getUserManager() {
        return userManager;
    }

    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }

    public IPasswordManager getPasswordManager() {
        return passwordManager;
    }

    public void setPasswordManager(PasswordManager passwordManager) {
        this.passwordManager = passwordManager;
    }

    public InterSessionContextBean getInterSessionContext() {
        return interSessionContext;
    }

    public void setInterSessionContext(InterSessionContextBean interSessionContext) {
        this.interSessionContext = interSessionContext;
    }
}
