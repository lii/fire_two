package se.gu.fire.security.shiro;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.PersistenceException;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.SimpleByteSource;

import se.gu.fire.core.FireUser;
import se.gu.fire.core.Password;
import se.gu.fire.data.inter.IPasswordManager;
import se.gu.fire.data.inter.IUserManager;
import se.gu.fire.util.FireLogger;
import se.gu.fire.util.FireUtil;

public class FireRealm extends AuthorizingRealm {

    private final IPasswordManager passwordManager;
    private final IUserManager userManager;

    /*
     * Database connectors
     */
    /*
     * The kind of login tokens which can be used in conjunction with our
     * Database Realm
     */
    private final List<AuthenticationToken> supportedTokens = Arrays.asList(new UsernamePasswordToken());

    
    public FireRealm() {
    	passwordManager = lookupBean(IPasswordManager.class);
    	userManager = lookupBean(IUserManager.class);
    }
    
    
    @Override
    public String getName() {
        return toString();
    }

    @Override
    public boolean supports(AuthenticationToken at) {

        FireLogger.logInfo("Checking for support of token {0} in Realm: {1}",at.getClass(), getName());

        for (AuthenticationToken token : supportedTokens) {
            if (at.getClass().equals(token.getClass())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return FireRealm.class.getName();
    }

    /*
     * The main authentication method. Check the authenticationtoken against the
     * database, and determine whether or not the user is applicable for logging
     * in.
     */
    @Override
    public AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken at)
            throws AuthenticationException {

        // Extract user credentials
        UsernamePasswordToken token = (UsernamePasswordToken) at;
        String username = token.getUsername();

        // Extract user from the database
        FireUser resolvedUser;
        try {
            resolvedUser = userManager.findUser(username);

        } catch (PersistenceException e) {
            FireLogger.logSevere("Realm {0} has encountered a database problem: {1}", getName(), e.getClass());
            throw new AuthenticationException("Database error: " + e.getMessage());
        }

        if (resolvedUser == null) {
            FireLogger.logSevere("Realm {0} found no match for user {1}", getName(), username);
            throw new UnknownAccountException("Unable to locate user: " + username);
        }

        // Extract password and check for match
        Password resolvedPassword;
        try {
            resolvedPassword = passwordManager.findPassword(resolvedUser);

        } catch (Exception e) {
            FireLogger.logSevere("Realm {0} has encountered a database problem: {1}", getName(), e.getClass());
            throw new AuthenticationException("Database error: " + e.getMessage());
        }

         // If the password does not exist, the user will have to create a new one
        if (resolvedPassword == null) {
            FireLogger.logSevere("Realm {0} found no matching password for user {1}", getName(), username);
            throw new UnknownAccountException("Password for account could not be found. New password must be created");
        }

        FireLogger.logInfo("Realm {0} generated SimpleAuthenticationToken for user {1}", getName(), username);

        return new SimpleAuthenticationInfo(resolvedUser.getEmail(),
                resolvedPassword.getPassword(),
                new SimpleByteSource(resolvedPassword.getSalt()),
                getName());
    }

    /**
     * The main authorization method. We here attempt to retrieve all the 
     * Roles and Permissions for the current user, and pass them back to Shiro
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection pc) {
        
        String currentUserEmail = (String) pc.getPrimaryPrincipal();
        HashSet<String> userRoles = new HashSet<>();
        
        if (currentUserEmail == null) {
            FireLogger.logSevere("No authorization info available for: {0}", FireUtil.getLoggedinUserEmail());
            return new SimpleAuthorizationInfo();
        }
        
        if (!currentUserEmail.equals(FireUtil.getLoggedinUserEmail())) {
            FireLogger.logSevere("WARNING: possible masquerader. Authorizing user is {0} but logged in user is {1}",
                    currentUserEmail,
                    FireUtil.getLoggedinUserEmail());
        }
        
        FireUser currentUser = userManager.findUser(currentUserEmail);
        
        if (currentUser == null) {
            FireLogger.logSevere("FATAL: attempting to find authorization info for non-existen user: {0}", FireUtil.getLoggedinUserEmail());
            return new SimpleAuthorizationInfo();
        }
        
        userRoles.add(currentUser.getRole().toString());
        FireLogger.logInfo("Returning authorization info for user {0}, with role {1}",
                currentUser.getEmail(),
                currentUser.getRole().toString());
 
        
        /*
        Collection<FireUser> users = pc.byType(FireUser.class);

        if (users.isEmpty()) {
            FireLogger.logInfo("No authorization info available for: " + FireUtil.getLoggedinUserEmail());
            return new SimpleAuthorizationInfo();
        }
        for (FireUser user : users) {
            userRoles.add(user.getRole().toString());
            FireLogger.logInfo("Attempting to obtain authorization info for user email: " + user.getEmail());
        }
        */
        
        return new SimpleAuthorizationInfo(userRoles);
    }
    
    @SuppressWarnings("unchecked")
	private <T> T lookupBean(Class<T> cls) {
        try {
        	BeanManager beanMan = (BeanManager) new InitialContext().lookup("java:comp/BeanManager");
	        Bean<T>  bean = (Bean<T>) beanMan.getBeans(cls).iterator().next();
	        return (T) beanMan.getReference(bean, cls, beanMan.createCreationalContext(bean));
        } catch (NamingException e) {
			throw new RuntimeException(e);
		}
    }

}