package se.gu.fire.core;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * A file to be stored in main memory.
 */
@Entity
public class StoredFile implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    private String fileName;
    private byte[] fileData;
    private String contentType;
    
    public StoredFile() {};
    
    public StoredFile(String fileName, byte[] fileData, String contentType) {
        this.fileName = fileName;
        this.fileData = fileData;
        this.contentType = contentType;
    }
    
    public Long getId() {
        return id;
    }

    public String getFileName() {
        return fileName;
    }

    public String getContentType() {
        return contentType;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public byte[] getFileData() {
        return fileData;
    }

    public void setFileData(byte[] fileData) {
        this.fileData = fileData;
    }

    @Override
    public String toString() {
        return "StoredFile{" + "id=" + id + ", fileName=" + fileName + '}';
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        
        final StoredFile other = (StoredFile) obj;
        if(this.id == null || other.id == null) {
            return false;
        }
            
        return this.id.equals(other.id);
        
    }

    @Override
    public int hashCode() {
        int hash = 31;
        hash = 59 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }
    
}