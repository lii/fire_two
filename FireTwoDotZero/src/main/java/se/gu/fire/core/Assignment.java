package se.gu.fire.core;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import se.gu.fire.backend.CreateAssignmentListener;

/**
 * Represents a task that the student must solve, hence Assignment
 *
 * An Assignment is problem that the student must solve. When the student
 * attempts to solve the assignment it is done with the Submission, that will
 * have a reference to the Assignment it is meant to solve. Assignment
 */
@EntityListeners({CreateAssignmentListener.class})
@Entity
public class Assignment implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotNull
    private String title;
    
    @NotNull
    private String information;
    
    @Temporal(TemporalType.DATE)
    private Date finalDeadline;
    
    @Temporal(TemporalType.DATE)
    private Date firstDeadline;

    public Assignment() {}
    
    public Assignment(Builder builder) {
        this.title = builder.title;
        this.information = builder.information;
        this.finalDeadline = builder.finalDeadline;
        this.firstDeadline = builder.firstDeadline;
    }

    public Long getId() {
        return id;
    }

    public Date getFirstDeadline() {
        return firstDeadline;
    }

    public void setFirstDeadline(Date firstDeadline) {
        this.firstDeadline = firstDeadline;
    }

    public Date getFinalDeadline() {
        return finalDeadline;
    }
    
    public Date getNextDeadline() {
        Date now = new Date();
        if(firstDeadline != null && now.before(firstDeadline)) {
            return firstDeadline;
        }
        
        return finalDeadline;

    }

    public void setFinalDeadline(Date deadLine) {
        this.finalDeadline = deadLine;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        final Assignment other = (Assignment) obj;
        if (this.id == null || other.id == null) {
            return false;
        }

        return this.id.equals(other.id);

    }

    @Override
    public String toString() {
        return "Assignment{" + "title=" + title + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }
    
    public static class Builder {
        private String title;
        private String information;
        private Date finalDeadline;
        private Date firstDeadline = null;
        
        public Builder() { }
        
        public Builder title(String value) {
            this.title = value;
            return this;
        }
        
        public Builder information(String value) {
            this.information = value;
            return this;
        }
        
        public Builder finalDeadline(Date value) {
            this.finalDeadline = value;
            return this;
        }
        
        public Builder firstDeadline(Date value) {
            this.firstDeadline = value;
            return this;
        }
        
        public Assignment build() {
            return new Assignment(this);
        }
    }
}