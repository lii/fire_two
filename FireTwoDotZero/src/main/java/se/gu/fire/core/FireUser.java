package se.gu.fire.core;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * Represents any user of the Fire system.
 *
 * This class can represent any user of the Fire system. 
 *
 * @see Admin
 * @see Grader
 */
@Entity
public class FireUser implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    private String fname;
    @NotNull
    private String lname;
    @NotNull
    @Column(unique = true)
    private String email;
    @NotNull
    @Enumerated(EnumType.STRING)
    private UserRole userRole;
    @Column(unique = true)
    private String personnummer;

    public FireUser(Builder builder) {
        this.fname = builder.fname;
        this.lname = builder.lname;
        this.email = builder.email;
        this.userRole = builder.userRole;
        this.personnummer = builder.personNummer;
    }

    public FireUser() {
        this.userRole = UserRole.STUDENT;
    }

    public String getFullName() {
        return getFname() + " " + getLname();
    }

    public Long getId() {
        return id;
    }

    public UserRole getRole() {
        return userRole;
    }

    public void setRole(UserRole role) {
        this.userRole = role;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPersonnummer() {
        return personnummer;
    }

    public void setPersonnummer(String personnummer) {
        this.personnummer = personnummer;
    }

    /**
     * Two FireUsers are equal if they have the same id. Otherwise they are not.
     * 
     * Christopher: Since email is also a unique identifier, and IDs are not always present,
     * we can check this as well...now I need to start running from Oscar.
     * No inheritance.
     *
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        final FireUser other = (FireUser) obj;
        if (this.id == null || other.id == null) {
            return this.email.equals(other.email);
        }

        return this.id.equals(other.id);

    }

    @Override
    public String toString() {
        return "FireUser{" + "id=" + id + ", email=" + email + '}';
    }

    @Override
    public int hashCode() {
        int hash = 17;
        hash = 59 * hash + ( this.id != null ? this.id.hashCode() : 0 );
        return hash;
    }

    public static class Builder {

        private String fname;
        private String lname;
        private String email;
        private String personNummer = null;
        private UserRole userRole = UserRole.STUDENT;

        public Builder() {
        }

        public Builder lastName(String value) {
            lname = value;
            return this;
        }

        public Builder firstName(String value) {
            fname = value;
            return this;
        }

        public Builder email(String value) {
            email = value;
            return this;
        }

        public Builder personNummer(String value) {
            personNummer = value;
            return this;
        }

        public Builder userRole(UserRole value) {
            userRole = value;
            return this;
        }

        public FireUser build() {
            return new FireUser(this);
        }
    }
}