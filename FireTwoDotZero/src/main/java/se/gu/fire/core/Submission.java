package se.gu.fire.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import se.gu.fire.util.FireUtil;

/**
 * Represents an attempt by a students to solve an Assignment
 *
 * A StudentGroup will create a new Submission when they try to solve an
 * Assignment. The Submission object represents an attempt to complete an
 * assignment.
 *
 * @see StudentGroup
 * @see Assignment
 *
 */
@Entity
public class Submission implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Enumerated(EnumType.STRING)
    private SubmissionStatus status;
    @OneToMany(cascade = CascadeType.ALL)
    private List<StoredFile> files = new ArrayList<>();
    @NotNull
    @OneToOne
    private Assignment assignment;
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date submitTime;
    @NotNull
    @OneToOne
    private StudentGroup submittingGroup;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Comment> comments = new ArrayList<>(0);
    // TODO: Kan vi fixa att bara graders kan tilldelas detta eller måste använda
    // defensiv programmering antar jag?
    @OneToOne
    private FireUser grader;

    /**
     * Represents the different states that a Submission object can be in.
     *
     * When first submitted an Submission object is new. When a TA then grades
     * the submission it will become either accepted or rejected.
     *
     * @see {@link Submission}
     */
    public enum SubmissionStatus {
        New, Accepted, Rejected, NotSubmitted;
    }

    public Submission() {
        this.submitTime = new Date();
        this.status = SubmissionStatus.New;
    }
    
    public Submission(Builder builder) {
        this.status = builder.status;
        this.submitTime = builder.submitTime;
        this.grader = builder.grader;
        this.assignment = builder.assignment;
        this.submittingGroup = builder.submittingGroup;
    }

    public List<StoredFile> getFiles() {
        return files;
    }

    public void addFile(StoredFile file) {
        this.files.add(file);
    }

    public void addFiles(Iterable<StoredFile> files) {
        for (StoredFile f : files) {
            addFile(f);
        }
    }

    public Assignment getAssignment() {
        return assignment;
    }

    public void setAssignment(Assignment assignment) {
        this.assignment = assignment;
    }

    public Long getId() {
        return id;
    }

    public StudentGroup getSubmittingGroup() {
        return submittingGroup;
    }

    public void setSubmittingGroup(StudentGroup studentGroup) {
        this.submittingGroup = studentGroup;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void addComments(Comment comment) {
        this.comments.add(comment);
    }

    public FireUser getGrader() {
        return grader;
    }

    public void setGrader(FireUser grader) {
        FireUtil.verifyArg(grader.getRole() != UserRole.STUDENT);
        this.grader = grader;
    }

    public SubmissionStatus getSubmissionStatus() {
        return status;
    }

    public void setSubmissionStatus(SubmissionStatus status) {
        this.status = status;
    }

    public Date getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(Date submitTime) {
        this.submitTime = submitTime;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        final Submission other = (Submission) obj;
        if (this.id == null || other.id == null) {
            return false;
        }

        return this.id.equals(other.id);
    }

    @Override
    public int hashCode() {
        int hash = 23;
        hash = 59 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("Submission{")
                .append("id=") 
                .append(id) 
                .append(", status=")
                .append(status)
                .append(", submitTime=")
                .append(submitTime)
                .append(", submittingGroup=")
                .append(submittingGroup)
                .append('}')
                .toString();
    }

    public static class Builder {

        private Assignment assignment;
        private StudentGroup submittingGroup;
        private SubmissionStatus status = SubmissionStatus.New;
        private Date submitTime = new Date();
        private FireUser grader = null;

        public Builder() {
        }

        public Builder assignment(Assignment value) {
            this.assignment = value;
            return this;
        }

        public Builder submittingGroup(StudentGroup value) {
            this.submittingGroup = value;
            return this;
        }

        public Builder grader(FireUser value) {
            this.grader = value;
            return this;
        }

        public Builder status(SubmissionStatus value) {
            this.status = value;
            return this;
        }

        public Builder sumitTime(Date value) {
            this.submitTime = value;
            return this;
        }

        public Submission build() {
            return new Submission(this);
        }
    }
}