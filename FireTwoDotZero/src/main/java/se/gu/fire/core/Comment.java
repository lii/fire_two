package se.gu.fire.core;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Represents a comment made regarding an Submission object.
 *
 * When a some user (student, grader or admin) writes a comment regarding a
 * submission, the comment is represented by this class.
 *
 */
@Entity
public class Comment implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private FireUser sender;

    private String comment;

    @Temporal(TemporalType.DATE)
    private Date created;

    public Comment() {
        this.created = new Date();
    }

    public Comment(FireUser sender, String comment) {
        this();
        this.sender = sender;
        this.comment = comment;
    }

    public Long getId() {
        return id;
    }

    public FireUser getSender() {
        return sender;
    }

    public void setSender(FireUser sender) {
        this.sender = sender;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        final Comment other = (Comment) obj;
        if(this.id == null || other.id == null) {
            return false;
        }

        return this.id.equals(other.id);

    }

    @Override
    public int hashCode() {
        int hash = 13;
        hash = 59 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

}