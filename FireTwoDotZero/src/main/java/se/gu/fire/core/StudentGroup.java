package se.gu.fire.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * In order to submit solutions to Assignments, students must join a group.
 *
 * This Entity represents a Student group that students will join in order
 * to be able create submissions.
 *
 * @see Submission
 */
@Entity
public class StudentGroup implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // Found what seems to be a bug in EclipseLink or Derby. 
    // Will crash without this field that isn't used.
    @SuppressWarnings("unused")
	private Long trash;
    
    @OneToMany
    List<FireUser> groupMembers = new ArrayList<>();

    public StudentGroup(){};

    public List<FireUser> getGroupMembers() {
        return groupMembers;
    }

    public void addGroupMember(FireUser groupMember) {
        this.groupMembers.add(groupMember);
    }

    public Long getId(){
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void addAllGroupMembers(Iterable<FireUser> itr) {
        for (FireUser user : itr) addGroupMember(user);
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        final StudentGroup other = (StudentGroup) obj;
        if(this.id == null || other.id == null) {
            return false;
        }

        return this.id.equals(other.id);

    }

    @Override
    public int hashCode() {
        int hash = 19;
        hash = 59 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "Group: " + id;
    }
}
