package se.gu.fire.core;

public enum UserRole {
    STUDENT, GRADER, ADMIN;
}
