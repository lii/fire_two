package se.gu.fire.core;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * A password mapping between a user id and a password.
 */
@Entity
public class Password implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @NotNull
    private String password;
    
    @NotNull
    private byte[] salt;

    public Password() {
    }

    public Password(Long id, String password) {
        this.id = id;
        this.password = password;
    }

    public Password(Long id, String password, byte[] salt) {
        this(id, password);
        this.salt = salt;
    }

    public byte[] getSalt() {
        return salt;
    }

    public void setSalt(byte[] salt) {
        this.salt = salt;
    }

    public Long getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Password{" + "id=" + id + ", password=" + password + '}';
    }
}
