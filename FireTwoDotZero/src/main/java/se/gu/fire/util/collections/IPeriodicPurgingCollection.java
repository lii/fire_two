/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.gu.fire.util.collections;

import java.util.concurrent.ScheduledThreadPoolExecutor;

/**
 * The purging collections implementing this interface use task scheduling
 * in order to periodically purge elements they contain, according to timestamps
 * attached to those elements. 
 * 
 * It's times like these you get deeply frustrated that Java does not support
 * either mixins or multiple inheritance...
 */
public interface IPeriodicPurgingCollection extends IPurgingCollection {
    
    public ScheduledThreadPoolExecutor purgeScheduler = new ScheduledThreadPoolExecutor(10); 
    public abstract class Purger implements Runnable {}
}
