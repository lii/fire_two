package se.gu.fire.util;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

/**
 * Various utility methods for more convenient work with JSF and PrimeFaces
 */
public class FireFaces {

    /**
     * Registers a PhaseListener at a moment suitible for updating beans that
     * depend on data gotten from view params.
     */
    public static void registerAfterValuesUpdate(UIViewRoot viewRoot, final Runnable r) {
        viewRoot.addPhaseListener(new PhaseListener() {
			private static final long serialVersionUID = 1L;

			@Override
            public void afterPhase(PhaseEvent event) {
                r.run();
            }

            @Override
            public void beforePhase(PhaseEvent event) {
            }

            @Override
            public PhaseId getPhaseId() {
                return PhaseId.UPDATE_MODEL_VALUES;
            }
        });
    }

    /*
     * Utility classes for smooth adding of messages to the Faces context
     */
    private static void addMessage(FacesMessage.Severity severity, String summary, String detail) {
        FacesMessage newMessage = new FacesMessage(severity, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, newMessage);
    }

    public static void addInfoMessage(String summary, String detail) {
        addMessage(FacesMessage.SEVERITY_INFO, summary, detail);
    }

    public static void addInfoMessage(String summary) {
        addMessage(FacesMessage.SEVERITY_INFO, summary, "");
    }

    public static void addWarningMessage(String summary, String detail) {
        addMessage(FacesMessage.SEVERITY_WARN, summary, detail);
    }

    public static void addWarningMessage(String summary) {
        addMessage(FacesMessage.SEVERITY_WARN, summary, "");
    }

    public static void addErrorMessage(String summary, String detail) {
        addMessage(FacesMessage.SEVERITY_ERROR, summary, detail);
    }

    public static void addErrorMessage(String summary) {
        addMessage(FacesMessage.SEVERITY_ERROR, summary, "");
    }

    public static void addFatalMessage(String summary, String detail) {
        addMessage(FacesMessage.SEVERITY_FATAL, summary, detail);
    }

    public static void addFatalMessage(String summary) {
        addMessage(FacesMessage.SEVERITY_FATAL, summary, "");
    }
}
