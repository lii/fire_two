package se.gu.fire.util;

/**
 * Various global constants to be used across the application
 */
public enum Consts {

    ;
    
        /*
         * Name of the default Persistence Unit
         */
    public static final String PU_NAME = "firedb";
}
