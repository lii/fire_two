package se.gu.fire.util;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Reduces the burden of logging by encapsulating prettyprinting and other 
 * stuff. Please use instead of the normal invocation. Threadsafe.
 */
public class FireLogger {

    public static void logInfo(String message) {
        Logger.getLogger(getClassName()).log(Level.INFO, "{0}: {1}", new Object[]{getClassName(), message});
    }

    public static void logInfo(String message, Object...params) {
        Logger.getLogger(getClassName()).log(Level.INFO, getClassName() + ": " + message, params);
    }
    
    public static void logWarning(String message) {
        Logger.getLogger(getClassName()).log(Level.WARNING, "{0}: {1}", new Object[]{getClassName(), message});
    }

    public static void logWarning(String message, Object...params) {
        Logger.getLogger(getClassName()).log(Level.WARNING, getClassName() + ": " + message, params);
    }

    public static void logSevere(String message) {
        Logger.getLogger(getClassName()).log(Level.SEVERE, "{0}: {1}", new Object[]{getClassName(), message});
    }

    public static void logSevere(String message, Object...params) {
        Logger.getLogger(getClassName()).log(Level.SEVERE, getClassName() + ": " + message, params);
    }

    /**
     * Retrieve the name of the class invoking the logger, by looking 
     * up the stacktrace.
     * 
     * @return 
     */
    private static String getClassName() {
        return Thread
                .currentThread()
                .getStackTrace()[3]
                .getClassName();
    }

}
