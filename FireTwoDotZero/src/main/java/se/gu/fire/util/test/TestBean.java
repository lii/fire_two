package se.gu.fire.util.test;

import com.google.common.collect.Lists;

import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import org.apache.shiro.SecurityUtils;

import se.gu.fire.backend.LoginBean;
import se.gu.fire.data.AbstractDAO;
import se.gu.fire.data.AssignmentManager;
import se.gu.fire.data.CommentManager;
import se.gu.fire.data.GroupManager;
import se.gu.fire.data.inter.IAssignmentManager;
import se.gu.fire.data.inter.ICommentManager;
import se.gu.fire.data.inter.IGroupManager;
import se.gu.fire.data.inter.IDataAccessO;
import se.gu.fire.data.inter.IPasswordManager;
import se.gu.fire.data.PasswordManager;
import se.gu.fire.data.inter.IStoredFileManager;
import se.gu.fire.data.StoredFileManager;
import se.gu.fire.data.SubmissionManager;
import se.gu.fire.data.UserManager;

/**
 * Manages test data an test operations.
 *
 * Writes test data to DB.
 *
 * Logins and navigates.
 *
 * UGLY HACK WARNING: This class is used both as a backing bean and from the
 * unit tests.
 */
@Named("testBean")
@RequestScoped
public class TestBean {

    @Inject
    private LoginBean loginBean;

    @EJB
    private IAssignmentManager assignMan;

    @EJB
    private SubmissionManager subMan;

    @EJB
    private ICommentManager comMan;

    @EJB
    private IGroupManager groupMan;

    @EJB
    private UserManager userMan;

    @EJB
    private IStoredFileManager fileMan;

    @EJB
    private IPasswordManager passMan;

    private List<IDataAccessO<?, ?>> mans;

    private EntityManager entMan;

    private Map<Class<?>, IDataAccessO<?, ?>> manMap = new LinkedHashMap<>();


    @PostConstruct
    public void init() {
        mans = collectMans();
        for (IDataAccessO<?, ?> man : mans) {
            manMap.put(man.getClass(), man);
        }
    }

    private static final Logger logger = Logger.getLogger(TestBean.class.getName());

    public TestBean() {}

    public TestBean(String puName) {
        this(Persistence.createEntityManagerFactory(puName).createEntityManager());
    }

    public TestBean(EntityManager entMan) {
        this.entMan = entMan;

        assignMan = new AssignmentManager();
        subMan = new SubmissionManager();
        userMan = new UserManager();
        groupMan = new GroupManager();
        comMan = new CommentManager();
        passMan = new PasswordManager();
        fileMan = new StoredFileManager();
        mans = collectMans();

        for (IDataAccessO<?, ?> man : mans) man.setEntityManager(entMan);
    }

    public void populateAndLogin(int userNr) {
        clearDb();
        populateDb();

        TestData testData = new TestData();

        loginBean.setUserEmail(testData.users.get(userNr).getEmail());
        loginBean.setUserPassword(testData.passwordStrs.get(userNr));

        SecurityUtils.getSubject().logout();
        loginBean.doLogin();
    }

    public void injectManagers(Object o) {
        Class<?> cls = o.getClass();

            for (Field f : cls.getFields()) {
                if (f.getAnnotation(EJB.class) != null
                        && AbstractDAO.class.isAssignableFrom(f.getType()) ) {

                    f.setAccessible(true);

                    if (!manMap.containsKey(f.getType())) {
                        throw new IllegalArgumentException("Type of field not a known manager: " + f.getType());
                    }

                    try {
                        f.set(o, manMap.get(f.getType()));
                    } catch (IllegalAccessException ex) {
                       throw new RuntimeException("Failed to inject manager. Object: " + o + " Field: " + f, ex);
                    }
                }
            }
    }
    
    public EntityManager getEntMan() {
        return entMan;
    }

    // Commented out because mail is not availible in unit test.
//    public void sendMail() {
//        logger.info("Sending mail");
//        try {
//            Mailer.send("mailout.comhem.se", "jensli@lavabit.com",
//                    Arrays.asList("firetwodotzero@googlegroups.com"),
//                    "Hello world!", "Från Jens Glassfish-server och Java Mail.");
//        } catch (MessagingException ex) {
//            logger.log(Level.SEVERE, null, ex);
//        }
//    }

    @SuppressWarnings("unchecked")
	public void populateDb() {
        logger.info("Populating the DB");

        TestData data = new TestData();

        begin();
        for (IDataAccessO<?, ?> man : mans) {
            createAll(
                    (List<Object>) data.allDataMap.get(man.getEntityClass()),
                    (IDataAccessO<Object, ?>) man);
        }
        commit();

        begin();
        data.createPasswords();
        createAll(data.passwords, passMan);
        commit();
    }

    private void begin() {
        if (entMan != null) entMan.getTransaction().begin();
    }

    private void commit() {
        if (entMan != null) entMan.getTransaction().commit();
    }

    private static <T> void createAll(List<T> entitis, IDataAccessO<T, ?> man) {
        for (T ent : entitis) man.create(ent);
    }
    
    public void clearDb() {
        if (entMan != null) entMan.clear();

        begin();
        for (IDataAccessO<?, ?> man : Lists.reverse(mans)) man.clear();
        commit();
    }
    
    private List<IDataAccessO<?, ?>> collectMans() {
            // NOTE: Order important
            return Lists.<IDataAccessO<?, ?>>newArrayList(
                passMan, userMan, comMan, groupMan, assignMan, fileMan, subMan);
    }


    public IAssignmentManager getAssignMan() {
        return assignMan;
    }

    public SubmissionManager getSubMan() {
        return subMan;
    }

    public ICommentManager getComMan() {
        return comMan;
    }

    public IGroupManager getGroupMan() {
        return groupMan;
    }

    public UserManager getUserMan() {
        return userMan;
    }

    public IStoredFileManager getFileMan() {
        return fileMan;
    }

    public IPasswordManager getPassMan() {
        return passMan;
    }

    public List<IDataAccessO<?, ?>> getMans() {
        return mans;
    }

}
