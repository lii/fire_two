package se.gu.fire.util.test;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.apache.shiro.util.ByteSource;
import se.gu.fire.core.Assignment;
import se.gu.fire.core.Comment;
import se.gu.fire.core.FireUser;
import se.gu.fire.core.Password;
import se.gu.fire.core.StoredFile;
import se.gu.fire.core.StudentGroup;
import se.gu.fire.core.Submission;
import se.gu.fire.core.Submission.SubmissionStatus;
import se.gu.fire.core.UserRole;

/**
 * Generic test data to simplify test case development
 */
public class TestData {

    public FireUser user1 =
            new FireUser.Builder()
            .firstName("Jens")
            .lastName("Li")
            .email("a@a.com")
            .personNummer("12345")
            .userRole(UserRole.STUDENT)
            .build(),
            user2 =
            new FireUser.Builder()
            .firstName("Oscar")
            .lastName("Utbult")
            .email("b@b.com")
            .personNummer("23456")
            .userRole(UserRole.STUDENT)
            .build(),
            user3 =
            new FireUser.Builder()
            .firstName("Poo")
            .lastName("Nubbe")
            .email("c@c.com")
            .personNummer("34567")
            .userRole(UserRole.ADMIN)
            .build(),
            user4 =
            new FireUser.Builder()
            .firstName("Peter")
            .lastName("Holm")
            .email("d@d.com")
            .personNummer("45678")
            .userRole(UserRole.GRADER)
            .build(),
            user5 =
            new FireUser.Builder()
            .firstName("Alexander")
            .lastName("Sjosten")
            .email("e@e.com")
            .personNummer("56789")
            .userRole(UserRole.GRADER)
            .build();
    public final List<String> passwordStrs = Lists.newArrayList(
            "a", "b", "c", "d", "e");
    public final List<FireUser> users = Lists.newArrayList(user1, user2, user3, user4, user5);
    public final List<StudentGroup> groups = Lists.newArrayList(
            new StudentGroup(), new StudentGroup());
    public final List<Assignment> assigns = Lists.newArrayList(
            new Assignment.Builder()
            .title("Ratnum labb")
            .information("Programmera RatNum labben ordentligt annars kommer CSvan.")
            .firstDeadline(getDate(7))
            .finalDeadline(getDate(14))
            .build(),
            new Assignment.Builder()
            .title("Passed labb")
            .information("Too late, alas, to late.")
            .firstDeadline(getDate(-7))
            .finalDeadline(getDate(-1))
            .build(),
            new Assignment.Builder()
            .title("Plupp labb")
            .information("Programmera Plupp labben ordentligt annars kommer Lii.")
            .firstDeadline(getDate(21))
            .finalDeadline(getDate(28))
            .build());
    public final List<Submission> subs = Lists.newArrayList(
            new Submission.Builder()
            .assignment(assigns.get(0))
            .submittingGroup(groups.get(0))
            .sumitTime(getDate(2.5))
            .build(),
            new Submission.Builder()
            .assignment(assigns.get(0))
            .submittingGroup(groups.get(0))
            .sumitTime(getDate(10.3))
            .build());
    /**
     * This is empty at creation time, becuase passwords must be constructed
     * after the users gets their IDs from the DB.
     */
    public final List<Password> passwords = Lists.newArrayList();
    /**
     * Contains all the data in the class, mapped from Class to a list of all
     * objects of that class.
     *
     * Can be used eg to automatically insert all data in a database.
     */
    public final Map<Class<?>, List<?>> allDataMap = Maps.newLinkedHashMap();
    public final List<StoredFile> files = Lists.newArrayList(
            new StoredFile("file1.txt", "1 Woo?".getBytes(), "text/plain"),
            new StoredFile("file2.txt", "1 Wha...!".getBytes(), "text/plain"),
            new StoredFile("file1.txt", "2 Woo?".getBytes(), "text/plain"),
            new StoredFile("file2.txt", "2 Wha...!".getBytes(), "text/plain"));
    public final List<Comment> comments = Lists.newArrayList(
            new Comment(user1, "Woo?"),
            new Comment(user1, "Whaa!"));
    public static Calendar cal = Calendar.getInstance();

    private static Date getDate(double t) {
        Calendar c = (Calendar) cal.clone();
        c.add(Calendar.HOUR, (int) t * 24);
        return c.getTime();
    }

    public TestData() {

        // Connect the objects with each other
        groups.get(0).addGroupMember(users.get(0));
        groups.get(0).addGroupMember(users.get(1));
        groups.get(1).addGroupMember(users.get(2));

        subs.get(0).setSubmissionStatus(SubmissionStatus.Rejected);
        subs.get(0).addFiles(Iterables.limit(files, 2));
        subs.get(0).addComments(comments.get(0));
        subs.get(0).addComments(comments.get(1));

        subs.get(1).setSubmissionStatus(SubmissionStatus.New);
        subs.get(1).addFiles(Iterables.skip(files, 2));

        allDataMap.put(FireUser.class, (List<?>) users);
        allDataMap.put(StoredFile.class, (List<?>) files);
        allDataMap.put(Assignment.class, (List<?>) assigns);
        allDataMap.put(Submission.class, (List<?>) subs);
        allDataMap.put(StudentGroup.class, (List<?>) groups);
        allDataMap.put(Password.class, (List<?>) passwords);
        allDataMap.put(Comment.class, (List<?>) comments);

    }

    /**
     * If they are going to be used in a database, the passwords must be created
     * after ther user has received their IDs, which happens when they are
     * inserted into the DB.
     */
    public void createPasswords() {
        int i = 0;
        for (FireUser user : users) {
            passwords.add(
                    new Password(
                    user.getId(),
                    new Sha256Hash(passwordStrs.get(i),
                    ByteSource.Util.bytes(new byte[]{7}), 1024).toBase64(),
                    new byte[]{7}));
            i++;
        }
    }
}
