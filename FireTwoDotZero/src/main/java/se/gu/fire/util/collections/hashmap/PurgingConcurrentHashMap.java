package se.gu.fire.util.collections.hashmap;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import se.gu.fire.util.FireLogger;
import se.gu.fire.util.collections.IPurgingCollection;

/**
 * Threadsafe Map which cleans itself of expired references periodically.
 * 
 * REFACTOR: use composition instead of inheritance...what a mess
 */
public class PurgingConcurrentHashMap<K, V>
        extends ConcurrentHashMap<K,V>
        implements IPurgingCollection {

    private static final long serialVersionUID = 1L;
    private long entryLifetime;
    private long purgeIntervalInMillis;
    private final ScheduledThreadPoolExecutor cleanupScheduler;
    private final ConcurrentHashMap<K, Date> timeStamps;
    private boolean enableLogging;

    public PurgingConcurrentHashMap() {
        this(900000);
    }

    public PurgingConcurrentHashMap(long cleaningIntervalInMillis) {
        this(cleaningIntervalInMillis, 20);
    }

    public PurgingConcurrentHashMap(long purgeIntervalInMillis, int corePoolCapacity) {
        this(purgeIntervalInMillis, corePoolCapacity, 5);
    }

    public PurgingConcurrentHashMap(long purgeIntervalInMillis, int corePoolCapacity, int initialCapacity) {
        this(purgeIntervalInMillis, corePoolCapacity, initialCapacity, 900000);
    }

    public PurgingConcurrentHashMap(long purgeIntervalInMillis, int corePoolCapacity, int initialCapacity, long entryLifetime) {
        super(initialCapacity);

        this.entryLifetime = entryLifetime;
        cleanupScheduler = new ScheduledThreadPoolExecutor(corePoolCapacity);
        timeStamps = new ConcurrentHashMap<>(initialCapacity);

        cleanupScheduler.scheduleWithFixedDelay(new Purger(), purgeIntervalInMillis, purgeIntervalInMillis, TimeUnit.MILLISECONDS);
    }

    @Override
    public V put(K key, V value) {
        V toReturn = super.put(key, value);
        timeStamps.put(key, getNewTimestamp());
        return toReturn;
    }

    @Override
    public V putIfAbsent(K key, V value) {
        V toReturn = super.putIfAbsent(key, value);
        if (toReturn == null) {
            timeStamps.put(key, getNewTimestamp());
        }
        return toReturn;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        for (Map.Entry<? extends K, ? extends V> entry : m.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public V remove(Object key) {
        V toReturn = super.remove(key);
        if (toReturn != null) {
            timeStamps.remove(key);
        }
        return toReturn;
    }

    @Override
    public V get(Object key) {
        return super.get(key);
    }

    @Override
    public void purge() {
        Date currentTime = Calendar.getInstance().getTime();

        for (K key : keySet()) {
            Date timeStamp = timeStamps.get(key);

            /*
             * Values not associated with a timestamp should be removed
             * silently, since they will not get cleaned in subsequent
             * runs of the cleaner. 
             * 
             * Otherwise, if the timestamp of the current key has expired,
             * remove it from both the timestamp index and our set of
             * entries.
             */
            if (timeStamp == null || currentTime.after(timeStamp)) {
                if (enableLogging) {
                    FireLogger.logInfo("Removing entry with key: {0}", key);
                }
                remove(key);
                continue;
            }

            /*
             * Otherwise, if the timestamp of the current key has expired,
             * remove it from both the timestamp index and our set of
             * entries.
             */
            if (currentTime.after(timeStamp)) {
                if (enableLogging) {
                    FireLogger.logInfo("Removing entry with key: {0}", key);
                }
                remove(key);
                timeStamps.remove(key);
            }
        }
    }

    public void setPurgeIntervalInMillis(long purgeIntervalInMillis) {
        this.purgeIntervalInMillis = purgeIntervalInMillis;
        cleanupScheduler.shutdownNow();
        cleanupScheduler.scheduleWithFixedDelay(new Purger(), purgeIntervalInMillis, purgeIntervalInMillis, TimeUnit.MILLISECONDS);
    }

    private final class Purger implements Runnable {

        @Override
        public void run() {
            purge();
        }
    }

    private Date getNewTimestamp() {
        return new Date(Calendar.getInstance().getTimeInMillis() + entryLifetime);
    }

    public void enableLogging(boolean enableLogging) {
        this.enableLogging = enableLogging;
    }

    public boolean loggingEnabled() {
        return enableLogging;
    }

    public long getEntryLifetime() {
        return entryLifetime;
    }

    public void setEntryLifetime(long entryLifetime) {
        this.entryLifetime = entryLifetime;
    }

    public long getPurgeIntervalInMillis() {
        return purgeIntervalInMillis;
    }

    public boolean isEnableLogging() {
        return enableLogging;
    }

    public void setEnableLogging(boolean enableLogging) {
        this.enableLogging = enableLogging;
    }
}
