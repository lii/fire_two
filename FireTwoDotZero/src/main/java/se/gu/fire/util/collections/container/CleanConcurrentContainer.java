package se.gu.fire.util.collections.container;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import se.gu.fire.util.FireLogger;
import se.gu.fire.util.collections.IPurgingCollection;
import se.gu.fire.util.collections.TimestampWrapper;

/**
 * A threadsafe container which periodically removes elements falling under a
 * given timestamp. The queue wraps a ConcurrentLinkedQueue for simplicity, and
 * hence does not have an inheritance releationships with either
 * ConcurrentLinkedQueue or Queues in general.
 */
public class CleanConcurrentContainer<T> implements IPurgingCollection {

    ConcurrentLinkedQueue<T> q;
    @SuppressWarnings("unused")
	private final long elementLifetimeInMillis;
    private final ScheduledThreadPoolExecutor cleanupScheduler;
    private final ConcurrentLinkedQueue<TimestampWrapper<T>> queue;
    private boolean enableLogging;

    public CleanConcurrentContainer() {
        this(900000, 900000);
    }

    public CleanConcurrentContainer(long elementLifetimeInMillis, long cleanupInterval) {

        this.elementLifetimeInMillis = elementLifetimeInMillis;

        cleanupScheduler = new ScheduledThreadPoolExecutor(5);
        queue = new ConcurrentLinkedQueue<>();

        cleanupScheduler.scheduleAtFixedRate(new Cleaner(), cleanupInterval, cleanupInterval, TimeUnit.MILLISECONDS);
    }

    /**
     * This is not entirely threadsafe (better would be to use a sorted queue,
     * but there are no concurrent ones).
     *
     * @param element
     */
    public void add(T element) {

        TimestampWrapper<T> wrappedElement =
                new TimestampWrapper<>(element);

        queue.add(wrappedElement);
    }

    public boolean contains(Object o) {

        /*
         * Not a good solution at all...no way to prevent classcastexceptions here
         * Except for forcing the basetype?
         */
        @SuppressWarnings("unchecked")
		TimestampWrapper<T> wrappedObject =
                new TimestampWrapper<>((T) o);

        try {
            return queue.contains(wrappedObject);
        }
        catch (Exception e) {
            return false;
        }
    }

    public void remove(Object o) {

        @SuppressWarnings("unchecked")
		TimestampWrapper<T> wrappedObject =
                new TimestampWrapper<>((T) o);

        try {
            queue.remove(wrappedObject);
        }
        catch (Exception e) {
        }
    }

    public void enableLogging(boolean enableLogging) {
        this.enableLogging = enableLogging;
    }

    public boolean loggingEnabled() {
        return enableLogging;
    }

    @Override
    public void purge() {
        Date currentTime = Calendar.getInstance().getTime();

        /*
         * Poll the queue while it still has elements. Stop as soon
         * as we encounter an element whose timestamp has NOT expired
         */
        while (!queue.isEmpty()) {

            TimestampWrapper<T> next = queue.peek();

            if (next.getTimestamp().before(currentTime)) {
                if (enableLogging) {
                    FireLogger.logInfo("Removing element: {0}", next.getElement());
                }
                queue.poll();
            } else {
                return;
            }
        }
    }

    private final class Cleaner implements Runnable {

        @Override
        public void run() {
            purge();
        }
    }
}
