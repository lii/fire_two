package se.gu.fire.util.collections.hashmap;

/**
 * Self-cleaning, thread-safe HashMap which requires a checksum test in order
 * for values to be retrieved.
 *
 * In the context of Fire, it is designed to provide several security benefits:
 *
 * 1. It prevents certain forms of flood attacks, such as the possibility of
 * flooding the HashMaps with duplicate values. Prevents is probably too strong
 * a word to use here, as existing hashmaps can obtain the same functionality.
 *
 * 2. It prevents unauthorized access to a given session context, by requiring a
 * validation number to access the session, apart from just an ID. We use a
 * random bytestring in order to make bruteforcing such a validation number (at
 * least somewhat) more difficult.
 */
public final class SecurePurgingConcurrentHashMap<K extends ISecureElement, V extends ISecureElement>
        extends PurgingConcurrentHashMap<K, V> {

    private static final long serialVersionUID = 1L;

    public SecurePurgingConcurrentHashMap() {
        super();
    }

    /**
     * Verifies checksum in order to return a value. A little uncomfortable with
     * this since the behavior is the same as if the user requested a
     * non-existent value, so not sure this is the best way to do it.
     *
     * @param key
     * @return
     */
    @Override
    public V get(Object key) {

        V value = super.get(key);

        if (value != null && value.belongsTo(key)) {
            return value;
        } else {
            return null;
        }
    }
}
