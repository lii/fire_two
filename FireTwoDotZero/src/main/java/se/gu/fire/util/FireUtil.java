package se.gu.fire.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import se.gu.fire.core.Assignment;
import se.gu.fire.core.StoredFile;
import se.gu.fire.core.Submission;
import se.gu.fire.core.Submission.SubmissionStatus;

public final class FireUtil {

    /**
     * Calculates whether it is allows to make a submission.
     * 
     * Returns null if it is allowed to make a submission on assign, given the
     * previous submissions in subs. Otherwise returns a String containing the
     * reason it is not allowed.
     */
    public static String calcBlockStatus(Assignment assign, List<Submission> subs, Date now) {
        if (now.after(assign.getFinalDeadline())) {
            return "Final deadline is passed";
        }

        for (Submission sub : subs) {
            if (sub.getSubmissionStatus() == SubmissionStatus.Accepted) {
                return "An accepted submission is allready made";
            }

            if (sub.getSubmissionStatus() == SubmissionStatus.New) {
                return "An ungraded submission is allready made";
            }
        }

        return null;
    }

    /**
     * Retrieve the prinicpal of the current user. In our case, this prinicpal
     * is an email address.
     * 
     * @return email of the logged in user
     */
    public static String getLoggedinUserEmail() {
        String email = (String) SecurityUtils.getSubject().getPrincipal();
        if (email == null) {
            FireLogger.logSevere("getLoggedinUserEmail returned null");
        }
        return email;
    }

    public static void verifyArg(boolean test) {
        if (!test) {
            throw new IllegalArgumentException("Illegal argument");
        }
    }

    public static void verifyArg(boolean test, String msg) {
        if (!test) {
            throw new IllegalArgumentException("Illegal argument: " + msg);
        }
    }

    public static void verifyState(boolean test, String msg) {
        if (!test) {
            throw new IllegalStateException("Illegal state: " + msg);
        }
    }

    /**
     * Create a zip file from a collection of files.
     * 
     * @param files files to compress
     * @return the zipfile
     */
    public static InputStream zipFiles(List<StoredFile> files) {
        ByteArrayOutputStream byteOut = new ByteArrayOutputStream();

        try (ZipOutputStream zipOut = new ZipOutputStream(byteOut)) {
            zipOut.setLevel(Deflater.NO_COMPRESSION);

            for (StoredFile file : files) {
                zipOut.putNextEntry(new ZipEntry(file.getFileName()));
                zipOut.write(file.getFileData());
                zipOut.closeEntry();
            }

        }
        catch (IOException ex) {
            throw new RuntimeException("Error while zipping", ex);
        }

        return new ByteArrayInputStream(byteOut.toByteArray());
    }

    /**
     * Generate a random string of alphanumeric characters
     * 
     * @param numberOfBytes number of bytes to generate the string from. Due
     * to the random nature of generated data and subsequent pruning, it is 
     * not guaranteed the resulting string will resemble the size of the input
     * @return the string of alphanumeric characters
     */
    public static String generateRandomByteKey(int numberOfBytes) {

        return new SecureRandomNumberGenerator()
                .nextBytes(numberOfBytes)
                .toBase64()
                .replaceAll("[^A-Za-z0-9]", "");
    }
}
