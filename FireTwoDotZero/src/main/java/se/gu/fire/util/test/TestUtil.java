package se.gu.fire.util.test;

import java.util.Random;

import se.gu.fire.core.FireUser;

public class TestUtil {

    private static final String[] firstNames = {"Kalle", "Anders", "Mackan", "Klas", "Oscar", "Jens", "Per", "Joachim", "Ana", "Eva", "Sandra", "Jonna", "Katarina", "Klara", "Lotta"};
    private static final String[] lastNames = {"Anka", "Utbult", "Lidestrom", "Andersson", "Svensson", "Karlsson", "Jacobsson", "Oscarsson", "Svantesson", "Von Hacht", "Reimers"};
    private static final Random randomGen = new Random();
    public static final String TEST_PU_NAME = "firedb_test";

    public static FireUser createRandomUser() {

        return new FireUser.Builder()
                .firstName(createFirstName())
                .lastName(createLastName())
                .email(createEmail())
                .personNummer(createPersonNummer())
                .build();
    }
    
    private static String createFirstName() {
        return firstNames[randomGen.nextInt(firstNames.length)];
    }
    
    private static String createLastName() {
        return lastNames[randomGen.nextInt(lastNames.length)];
    }

    private static String createEmail() {

        return "fireuser"
                + randomGen.nextInt(9999999)
                + "@fire.se";
        
    }

    private static String createPersonNummer() {

        return ""
                + randomGen.nextInt(100)
                + randomGen.nextInt(13)
                + randomGen.nextInt(29)
                + randomGen.nextInt(10000);
    }
}
