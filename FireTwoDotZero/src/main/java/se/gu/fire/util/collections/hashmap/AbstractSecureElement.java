package se.gu.fire.util.collections.hashmap;

import java.util.Objects;

/*
 * Base class for elements meant to be stored as part of a secure HashMap.
 * SecureElements contain a checksum which verifies
 * if a caller is allowed to retrieve them, in order to prevent spoofing.
 */
public abstract class AbstractSecureElement implements ISecureElement {

    private final String checksum;

    public final String getChecksum() {
        return checksum;
    }

    public AbstractSecureElement(String checksum) {
        this.checksum = checksum;
    }

    /**
     * Check if the other element is the sister to this one
     */
    public final boolean belongsTo(Object other) {
        
        if (other == null || !(other instanceof AbstractSecureElement)) {
            return false;
        }

        AbstractSecureElement resolvedOther = (AbstractSecureElement) other;
        
        return resolvedOther.getChecksum().equals(this.checksum);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.checksum);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractSecureElement other = (AbstractSecureElement) obj;
        return Objects.equals(this.checksum, other.checksum);
        
    }
    
}
