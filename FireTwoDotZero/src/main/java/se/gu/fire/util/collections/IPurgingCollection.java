package se.gu.fire.util.collections;

/*
 * Datastructures (both maps and regular collections) implementing this interface are 
 * capable of automatically and periodically purge their elements according to 
 * some criteria. They are efficient, for example, for contexts where data has 
 * to persist for an unknown period of time, but no longer than a given deadline 
 * in order to avoid clogging.
 */
public interface IPurgingCollection {
   
    /**
     * Purge some or all of the elements in this collection
     */
    public void purge();
}
