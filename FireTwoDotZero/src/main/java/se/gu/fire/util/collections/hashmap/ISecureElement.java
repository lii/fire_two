package se.gu.fire.util.collections.hashmap;

public interface ISecureElement {
    public boolean belongsTo(Object other);

}
