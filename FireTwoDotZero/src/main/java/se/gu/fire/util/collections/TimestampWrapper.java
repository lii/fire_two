/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.gu.fire.util.collections;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

/**
 * Wraps an element with a timestamp value
 */
public final class TimestampWrapper<T> {

    private final T element;
    private final Date timestamp;

    public TimestampWrapper(T element) {
        this(element, 0);
    }
    
    public TimestampWrapper(T element, long elementLifetimeInMillis) {
        this.element = element;
        this.timestamp = new Date(Calendar.getInstance().getTimeInMillis() + elementLifetimeInMillis);
    }

    public T getElement() {
        return element;

    }

    public Date getTimestamp() {
        return timestamp;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        @SuppressWarnings("unchecked")
		final TimestampWrapper<T> other = (TimestampWrapper<T>) obj;
        if (!Objects.equals(this.element, other.element)) {
            return false;
        }
        return true;
    }
}
