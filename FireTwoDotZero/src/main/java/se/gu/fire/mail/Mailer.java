package se.gu.fire.mail;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Provides static methods for sending mail from the system, to one or many
 * clients.
 */
public class Mailer {

    /*
     * Username and password for accessing the SMTP server. We use Gmail as
     * our default server.
     */
    private static String username = "reduxfire@gmail.com";
    private static String password = "firetwodotzero";

    /**
     * Send an email to a group of recipients
     *
     * @param recipients the recipients of the email
     * @param subject subject line of the email
     * @param message body of the email
     * @throws MessagingException
     */
    public static void send(List<String> recipients, String subject, String message)
            throws MessagingException {

        /*
         * Create an SMTP session to be used for sending the mail.
         */
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
        
        /*
         * Create a message to be sent over the session.
         */
        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(username));
        
        
        /*
         * Configure the recipient addresses, subject line and message body
         * and attach them to the message. Finally, send the message.
         */
        InternetAddress[] addressTo = new InternetAddress[recipients.size()];
        for (int i = 0; i < recipients.size(); i++) {
            addressTo[i] = new InternetAddress(recipients.get(i));
        }
        msg.setRecipients(Message.RecipientType.TO, addressTo);        
        msg.setSubject(subject);
        msg.setContent(message, "text/plain");

        Transport.send(msg);
    }

    /**
     * Send a mail to a single recipient
     *
     * @param recipient recipient of the email
     * @param subject subject line of the email
     * @param message message body of the email
     * @throws MessagingException
     */
    public static void send(String recipient, String subject, String message)
            throws MessagingException {

        ArrayList<String> recipients = new ArrayList<>();
        recipients.add(recipient);

        send(recipients, subject, message);
    }
}
