package se.gu.fire;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import se.gu.fire.core.FireUser;
import se.gu.fire.core.UserRole;

public class UserBuilderTest {
    
    public UserBuilderTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testBuilder() {
        FireUser user = new FireUser.Builder()
                .firstName("Malin")
                .lastName("Bergsten")
                .email("malin@chalmers.se")
                .personNummer("8301012345")
                .userRole(UserRole.GRADER)
                .build();
        
        assertNotNull(user);
    
    }
}
