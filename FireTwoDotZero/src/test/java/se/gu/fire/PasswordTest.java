package se.gu.fire;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import org.apache.shiro.crypto.RandomNumberGenerator;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.util.ByteSource;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import se.gu.fire.core.FireUser;
import se.gu.fire.core.Password;
import se.gu.fire.data.PasswordManager;
import se.gu.fire.data.UserManager;
import se.gu.fire.util.test.TestUtil;
import se.gu.fire.util.test.TestBean;

/**
 * Test setting passwords
 */
public class PasswordTest {

    private static EntityManager em;
    private static TestBean testBean;

    @BeforeClass
    public static void setUpClass() throws Exception {
        em = Persistence.createEntityManagerFactory(TestUtil.TEST_PU_NAME).createEntityManager();
        testBean = new TestBean(em);
    }
    
    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
        testBean.clearDb();
    }

    @After
    public void tearDown() {
        testBean.clearDb();
    }

    @Test
    public void insertAndGetPassword() {
        PasswordManager pm = new PasswordManager();
        pm.setEntityManager(em);
        
        UserManager um = new UserManager();
        um.setEntityManager(em);
        
        em.getTransaction().begin();
        Password password = new Password(10L, "tio");
        pm.create(password);
        em.getTransaction().commit();

        assertNotNull(pm.read(10L));

    }
    
    @Test
    public void associatePasswordWithUser() {
        PasswordManager pm = new PasswordManager();
        pm.setEntityManager(em);
        
        UserManager um = new UserManager();
        um.setEntityManager(em);
        
        FireUser oscar = TestUtil.createRandomUser();

        em.getTransaction().begin();
        um.create(oscar);
        em.getTransaction().commit();
        
        FireUser u5 = um.read(oscar.getId());
        assertNotNull(u5);

        String smart = "haxx";
        Password oscarsPassword = new Password(oscar.getId(), smart);
        em.getTransaction().begin();
        pm.create(oscarsPassword);
        em.getTransaction().commit();
        
        assertNotNull(pm.findPassword(oscar, smart));
        assertNull(pm.findPassword(oscar, "ohack"));

    }
    
    @Test
    public void testSaltGenerator() {
    
        RandomNumberGenerator numGen = new SecureRandomNumberGenerator();
        ByteSource src = numGen.nextBytes();
        
        System.err.println(src);
        System.err.println(src.getBytes());
        System.err.println(src.getClass());
    }
    
}
