package se.gu.fire;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import se.gu.fire.core.Assignment;
import se.gu.fire.core.Comment;
import se.gu.fire.core.FireUser;
import se.gu.fire.core.StoredFile;
import se.gu.fire.core.StudentGroup;
import se.gu.fire.core.Submission;
import se.gu.fire.core.Submission.SubmissionStatus;
import se.gu.fire.data.AssignmentManager;
import se.gu.fire.data.CommentManager;
import se.gu.fire.data.GroupManager;
import se.gu.fire.data.SubmissionManager;
import se.gu.fire.data.UserManager;
import se.gu.fire.util.test.TestBean;
import se.gu.fire.util.test.TestUtil;

public class SubmissionTest {

    private static EntityManager em;
    private static TestBean testBean;

    @BeforeClass
    public static void setUpClass() throws Exception {
        em = Persistence.createEntityManagerFactory(TestUtil.TEST_PU_NAME).createEntityManager();
        testBean = new TestBean(em);
    }

    public SubmissionTest() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        testBean.clearDb();
    }

    @After
    public void tearDown() {
        testBean.clearDb();
    }

    @Test
    public void testingSubmissions() {

        // Create the managers
        AssignmentManager am = new AssignmentManager();
        am.setEntityManager(em);

        SubmissionManager sm = new SubmissionManager();
        sm.setEntityManager(em);

        UserManager um = new UserManager();
        um.setEntityManager(em);

        GroupManager gm = new GroupManager();
        gm.setEntityManager(em);

        CommentManager cm = new CommentManager();
        cm.setEntityManager(em);

        // Fix assignment
        Assignment assign = new Assignment.Builder()
                .title("Ratnum labb")
                .information("Programmera RatNum labben ordentligt annars kommer Christopher.")
                .finalDeadline(new Date(Calendar.getInstance().getTime().getTime()+15000))
                .firstDeadline(new Date(Calendar.getInstance().getTime().getTime()+10000))
                .build();
            
        em.getTransaction().begin();
        am.create(assign);
        em.getTransaction().commit();


        // Fix users
        FireUser u = TestUtil.createRandomUser();
        
        em.getTransaction().begin();
        um.create(u);
        em.getTransaction().commit();


        FireUser u2 = TestUtil.createRandomUser();
        
        em.getTransaction().begin();
        um.create(u2);
        em.getTransaction().commit();

        // Student group
        StudentGroup group = new StudentGroup();
        em.getTransaction().begin();
        group.addGroupMember(u);
        gm.create(group);
        em.getTransaction().commit();

        assertEquals(SubmissionStatus.NotSubmitted, sm.getAssignmentStatus(assign, group));


        // Test adding submission and a file
        byte[] text = "Java, Lava\nNava".getBytes();
        
        Submission sub = new Submission.Builder()
                .assignment(assign)
                .submittingGroup(group)
                .grader(u2)
                .build();
        
        em.getTransaction().begin();
        sm.create(sub);
        em.getTransaction().commit();
        assertEquals(SubmissionStatus.New, sm.getAssignmentStatus(assign, group));

        Comment com = new Comment(u2, "Total katastrof! Bara Christopher kan rädda dig nu");

        sub.addComments(com);
        sub.addFile(new StoredFile("Hacker.java", text, "text/plain"));
        Submission sub2 = sm.read(sub.getId());

        assertEquals(sub, sub2);

        // Test setSubmissionStatus()
        sub.setSubmissionStatus(SubmissionStatus.Rejected);
        em.getTransaction().begin();
        sm.update(sub);
        em.getTransaction().commit();

        assertEquals(SubmissionStatus.Rejected, sm.getAssignmentStatus(assign, group));

        // Ensure comment exits
        assertTrue(sub2.getComments().size() > 0);

        // Ensure files exists
        List<StoredFile> files = sm.getSubmissionFiles(sub);
        assertTrue(files.size() > 0);

        sub = sm.read(sub.getId());
        assertEquals(SubmissionStatus.Rejected, sub.getSubmissionStatus());

        // Test getAllByGroup()
        Submission subAgain = new Submission.Builder()
                .assignment(assign)
                .submittingGroup(group)
                .grader(u2)
                .build();

        em.getTransaction().begin();
        sm.create(subAgain);
        em.getTransaction().commit();
        List<Submission> allGroupSub = sm.getAllByGroup(group);
        assertNotNull(allGroupSub);
        assertTrue(allGroupSub.size() >= 2);

        // Test getALlByGroup(Assignment, Group)
        allGroupSub = sm.getAllByGroup(assign, group);
        assertNotNull(allGroupSub);
        assertTrue(allGroupSub.size() >= 2);

        // Test submission with no grader doesn't give NPE
        subAgain = new Submission.Builder()
                .assignment(assign)
                .submittingGroup(group)
                .build();

        em.getTransaction().begin();
        sm.create(subAgain);
        em.getTransaction().commit();
        
        List<Submission> assignedSub = sm.getAllForGrader(assign, u2);
        assertNotNull(assignedSub);
        assertTrue(assignedSub.size() >= 2);

        // Test for isPassed
        subAgain.setSubmissionStatus(SubmissionStatus.Accepted);

        em.getTransaction().begin();
        sm.update(subAgain);
        em.getTransaction().commit();
        
        assertTrue(sm.isPassed(assign, group));      
        assertEquals(SubmissionStatus.Accepted, sm.getAssignmentStatus(assign, group));

        // Testa att group inte klarat kursen
        //assertFalse(am.isCompletedCourse(group));

    }
}
