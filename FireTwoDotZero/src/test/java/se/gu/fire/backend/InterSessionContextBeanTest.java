/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.gu.fire.backend;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import se.gu.fire.backend.intersession.CredentialsTuple;
import se.gu.fire.backend.intersession.InterSessionContextBean;
import se.gu.fire.core.FireUser;
import se.gu.fire.util.test.TestUtil;

/**
 *
 * @author christopher
 */
public class InterSessionContextBeanTest {

    InterSessionContextBean interSessionContextBean;

    public InterSessionContextBeanTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

        interSessionContextBean = new InterSessionContextBean();
    }

    @After
    public void tearDown() {
    }

    /**
     * Test the insertion of registration session data.
     */
    @Test
    public void testAddAndGetRegistrationSessionData() {
        
        /*
         * Add user data to context
         */
        FireUser user = TestUtil.createRandomUser();
        String password = "superhaxx";
        String sessionKey = interSessionContextBean.addRegistrationSessionData(user, password);
        
        /*
         * Assert it is present
         */
        CredentialsTuple credentialsTuple = interSessionContextBean.getRegistrationSessionData(sessionKey);
        assertTrue(credentialsTuple.getUser().equals(user));
        assertTrue(credentialsTuple.getPassword().equals(password));
    }

    /**
     * Test of addPasswordRecoverySessionData method, of class InterSessionContextBean.
     */
    @Test
    public void testAddPasswordRecoverySessionData() {
        
        /*
         * Add recovery session data to context
         */
    }

    /**
     * Test of verifyPasswordRecoverySessionData method, of class
     * InterSessionContextBean.
     */
    @Test
    public void testVerifyPasswordRecoverySessionData() {
        
        assertTrue(true);
        
        System.out.println("verifyPasswordRecoverySessionData");
        long userID = 0L;
        String key = "";
        InterSessionContextBean instance = new InterSessionContextBean();
        boolean expResult = false;
 
        //assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
}
