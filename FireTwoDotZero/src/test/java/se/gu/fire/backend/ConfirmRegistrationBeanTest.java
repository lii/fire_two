/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.gu.fire.backend;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import se.gu.fire.backend.intersession.InterSessionContextBean;
import se.gu.fire.core.FireUser;
import se.gu.fire.util.test.TestUtil;

/**
 *
 * @author christopher
 */
public class ConfirmRegistrationBeanTest {

    private InterSessionContextBean interSessionContextBean;
    private ConfirmRegistrationBean confirmRegistrationBean;
    private static EntityManager em;

    public ConfirmRegistrationBeanTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        interSessionContextBean = new InterSessionContextBean();
        confirmRegistrationBean = new ConfirmRegistrationBean();
        em = Persistence.createEntityManagerFactory(TestUtil.TEST_PU_NAME).createEntityManager();
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of doFinishRegistration method, of class ConfirmRegistrationBean.
     */
    @Test
    public void testDoFinishRegistration() {

        FireUser user = TestUtil.createRandomUser();
        String password = "haxx";

        interSessionContextBean.addRegistrationSessionData(user, password);
        confirmRegistrationBean.setPasswordManager(null);
        
        
    }
}
