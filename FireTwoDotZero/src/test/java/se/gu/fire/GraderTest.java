package se.gu.fire;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import se.gu.fire.core.FireUser;
import se.gu.fire.data.UserManager;
import se.gu.fire.util.test.TestUtil;
import se.gu.fire.core.UserRole;
import se.gu.fire.util.test.TestBean;

public class GraderTest {

    private static EntityManager em;
    private static TestBean testBean;

    @BeforeClass
    public static void setUpClass() throws Exception {
        em = Persistence.createEntityManagerFactory(TestUtil.TEST_PU_NAME).createEntityManager();
        testBean = new TestBean(em);
    }
    
    public GraderTest() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        testBean.clearDb();
    }

    @After
    public void tearDown() {
        testBean.clearDb();
    }

    @Test
    public void assignmentAndGrading() {
        UserManager um = new UserManager();
        um.setEntityManager(em);

        // Skapa ny grader
        FireUser u = TestUtil.createRandomUser();
        u.setRole(UserRole.GRADER);
        
        em.getTransaction().begin();
        um.create(u);
        em.getTransaction().commit();
        
        assertTrue(um.isGrader(u.getId()));
        assertTrue(um.isGrader(u));

        // Skapa en ny admin
        u = TestUtil.createRandomUser();
        u.setRole(UserRole.ADMIN);
        
        em.getTransaction().begin();
        um.create(u);
        em.getTransaction().commit();
        
        assertTrue(um.isAdmin(u.getId()));
        assertTrue(um.isAdmin(u));
    }
}
