package se.gu.fire;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.RollbackException;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import se.gu.fire.core.FireUser;
import se.gu.fire.data.UserManager;
import se.gu.fire.util.test.TestUtil;
import se.gu.fire.util.test.TestBean;



public class UserTest {
    
    private Collection<Object> rollbackInformation = new ArrayList<>();
    private static EntityManager em;
    private static TestBean testBean;
    
    @BeforeClass
    public static void setUpClass() throws Exception {
        em = Persistence.createEntityManagerFactory(TestUtil.TEST_PU_NAME).createEntityManager();
        testBean = new TestBean(em);
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
        testBean.clearDb();
    }

    @After
    public void tearDown() {
        testBean.clearDb();
    }
    
    @Test (expected=RollbackException.class)
    public void ensureUnique() {

        
        UserManager um = new UserManager();
        um.setEntityManager(em);
        
        String email = "notUnique@hotmail.com";
        
          // Test getting same item back!
        FireUser user = TestUtil.createRandomUser();
        user.setEmail(email);

        em.getTransaction().begin();
        um.create(user);
        em.getTransaction().commit();
        
        user = TestUtil.createRandomUser();
        user.setEmail(email);
        
        em.getTransaction().begin();
        um.create(user);
        em.getTransaction().commit();
        
    }

    @Test
    public void insertAndFetchUser() {
        
        UserManager userMan = new UserManager();
        userMan.setEntityManager(em);
        
          // Test getting same item back using email!
        String johnEmail = "aa@a.com";
        
        FireUser user1 =  TestUtil.createRandomUser();
        user1.setEmail(johnEmail);
        
        List<FireUser> us = userMan.getAll();
        for (FireUser u : us) System.out.println("User: " + u.getEmail());
        
        em.getTransaction().begin();
        userMan.create(user1);
        em.getTransaction().commit();

        // Lets find per email
        FireUser user = userMan.findUser(johnEmail);
        assertNotNull(user);
        
        List<FireUser> users = userMan.findUsersByFirstName(user1.getFname());
        assertTrue(users.contains(user1));
        
        users = userMan.findUsersByLastName(user1.getLname());
        assertTrue(users.contains(user1));
        
        FireUser user2 = userMan.read(user1.getId());

        assertNotNull(user2);
        assertTrue(user1.equals(user2));

        // Add 10 users to ensure we have enought
        // users for following tests
        em.getTransaction().begin();
        for(int i = 0; i < 10; i++) {
            userMan.create(new FireUser());
        }
        em.getTransaction().commit();
        
        // Get all
        List<FireUser> all = userMan.getAll();
        assertTrue(all.size() >= 10);

        // Get range tests
        all = userMan.getRange(0, 5);
        assertTrue(all.size() == 5);

        all = userMan.getRange(5, 5);
        assertTrue(all.size() == 5);

        // Test update
        String name = user1.getFname();
        user2.setFname("Another");
        em.getTransaction().begin();
        userMan.update(user2);
        em.getTransaction().commit();
        
        user2 = userMan.read(user1.getId());
        assertFalse(name.equals(user2.getFname()));

        // Test remove
        em.getTransaction().begin();
        userMan.delete(user2);
        em.getTransaction().commit();
        user2 = userMan.read(user1.getId());
        assertNull(user2);
              
    }

}
