/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.gu.fire;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import se.gu.fire.backend.intersession.SecureSessionElement;
import se.gu.fire.util.collections.hashmap.SecurePurgingConcurrentHashMap;

/**
 *
 * @author christopher
 */
public class SecureHashMapTest {
    
    public SecureHashMapTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
 
    @Test
    public void testInsert() {
        
        SecureSessionElement<Integer> key = new SecureSessionElement<>(123,"abc");
        SecureSessionElement<Integer> value = new SecureSessionElement<>(345,"abc");
        
        SecurePurgingConcurrentHashMap<SecureSessionElement<Integer>,SecureSessionElement<Integer>> map = new SecurePurgingConcurrentHashMap<>();
        
        assertTrue(map.put(key, value) == null);
        assertTrue(map.put(key, value) != null);
    }
    
    @Test
    public void testGet() {
        
        SecureSessionElement<Integer> key = new SecureSessionElement<>(123,"abc");
        SecureSessionElement<Integer> value = new SecureSessionElement<>(345,"abc");
        
        SecurePurgingConcurrentHashMap<SecureSessionElement<Integer>,SecureSessionElement<Integer>> map = new SecurePurgingConcurrentHashMap<>();
        
        map.put(key, value);
        
        SecureSessionElement<Integer> result = map.get(key);
        assertTrue(result != null);
        
        SecureSessionElement<Integer> bad = new SecureSessionElement<>(666,"dvl");
        result = map.get(bad);
        assertTrue(result == null);
        
        SecureSessionElement<Integer> identical = new SecureSessionElement<>(123,"abc");
        result = map.get(identical);
        assertTrue(result != null);
        
    }
}
