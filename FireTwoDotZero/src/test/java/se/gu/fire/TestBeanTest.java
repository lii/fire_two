package se.gu.fire;

import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import se.gu.fire.util.test.TestUtil;
import se.gu.fire.util.test.TestBean;

public class TestBeanTest {

    private static TestBean testBean;

    @BeforeClass
    public static void setUpClass() throws Exception {
        testBean = new TestBean(
                Persistence.createEntityManagerFactory(TestUtil.TEST_PU_NAME)
                    .createEntityManager());

        testBean.clearDb();

    }

    @Test
    public void populate() {

        testBean.populateDb();

        assertTrue(testBean.getUserMan().getAll().size() > 1);
    }

    @Test
    public void clear() {
        testBean.clearDb();

        assertTrue(testBean.getUserMan().getAll().isEmpty());
    }

    public TestBeanTest() {
    }

    @AfterClass
    public static void tearDownClass() {
        testBean.clearDb();
        testBean.getEntMan().close();
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
}