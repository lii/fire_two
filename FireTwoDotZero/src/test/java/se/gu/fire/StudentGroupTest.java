package se.gu.fire;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import se.gu.fire.core.FireUser;
import se.gu.fire.core.StudentGroup;
import se.gu.fire.data.GroupManager;
import se.gu.fire.data.UserManager;
import se.gu.fire.util.test.TestUtil;
import se.gu.fire.util.test.TestBean;
import se.gu.fire.util.test.TestData;

public class StudentGroupTest {

    private static EntityManager em;
    private static TestBean testBean;

    @BeforeClass
    public static void setUpClass() throws Exception {
        em = Persistence.createEntityManagerFactory(TestUtil.TEST_PU_NAME).createEntityManager();
        testBean = new TestBean(em);
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
        testBean.clearDb();
    }

    @After
    public void tearDown() {
        testBean.clearDb();
    }

    @Test
    public void someStudentGroupTests() {

        UserManager userMan = new UserManager();
        userMan.setEntityManager(em);
        
        GroupManager groupMan = new GroupManager();
        groupMan.setEntityManager(em);
        
        FireUser user1 = TestUtil.createRandomUser();

        StudentGroup sg = new StudentGroup();

        sg.addGroupMember(user1);

        em.getTransaction().begin();
        userMan.create(user1);
        groupMan.create(sg);
        em.getTransaction().commit();

        FireUser user2 = userMan.read(user1.getId());
        assertNotNull(user2);

        StudentGroup group = groupMan.read(sg.getId());
        assertNotNull(group);

        assertEquals(user2, user1);
        
        // Test getGroupForUser
        List<FireUser> users = groupMan.getGroupMembers(group);
        assertTrue(users.size() > 0);

        TestData data = new TestData();

        StudentGroup group2 = new StudentGroup();
        group2.addAllGroupMembers(data.users);

        em.getTransaction().begin();
        for (FireUser user : data.users) userMan.create(user);
        groupMan.create(group2);
        em.getTransaction().commit();

        StudentGroup group3 = groupMan.getGroupForUser(data.user2);

        assertEquals(group2, group3);
    }

}
