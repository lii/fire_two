package se.gu.fire;

import java.util.Calendar;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import se.gu.fire.core.Assignment;
import se.gu.fire.data.AssignmentManager;
import se.gu.fire.util.test.TestUtil;
import se.gu.fire.util.test.TestBean;

public class AssignmentTest {

    private static EntityManager em;
    private static TestBean testBean;

    @BeforeClass
    public static void setUpClass() throws Exception {
        em = Persistence.createEntityManagerFactory(TestUtil.TEST_PU_NAME).createEntityManager();
        testBean = new TestBean(em);
    }
    
    public AssignmentTest() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        testBean.clearDb();
    }

    @After
    public void tearDown() {
        testBean.clearDb();
    }

    @Test
    public void testAssignment() {
        AssignmentManager am = new AssignmentManager();
        am.setEntityManager(em);
        
        
        
        Assignment assign = new Assignment.Builder()
                .title("Ratnum labb")
                .information("Programmera RatNum labben ordentligt annars kommer Christopher.")
                .finalDeadline(new Date(Calendar.getInstance().getTime().getTime()+15000))
                .firstDeadline(new Date(Calendar.getInstance().getTime().getTime()+10000))
                .build();
            
        em.getTransaction().begin();
        am.create(assign);
        em.getTransaction().commit();
        
        Assignment assign2 = am.read(assign.getId());
        assertEquals(assign, assign2);
    }


}
