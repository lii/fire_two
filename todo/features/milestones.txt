Milestone 1
Anv�ndaren skall kunna skapa konto, kunna ladda upp text filer och visa dessa. 

Milestone 2
Skapa root konto: l�raren. L�raren skall kunna skapa separata uppgifter med deadline. Anv�ndaren laddar nu upp f�r varje uppgift. M�ts vid inloggning av en lista med uppgifter.

Milestone 3  
Skapa superuser konto: r�ttaren. R�ttaren kan reviewa inl�mningar och v�lja att acceptera eller rejecta dessa. Anv�ndaren ser visuellt om n�got �r rejectat eller inte. 

L�raren kan v�lja hur r�ttare skall tilldelas: viktat, j�mn, eller elev v�ljer. 

Milestone 4

B�rja implementera roliga features: t ex 

* Kontroll av inl�mingsuppgifter (se separat dokument)
* Loggning (se separat dokument)
* Checklistor f�r avklarade labbar som r�ttarna kan kryssa i. (se separat dokument)
* Implementera l�rarens kontrollpanel! Kan delegera f�rm�gor till r�ttarna som t ex extenda deadlines.

