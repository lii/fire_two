Links with parameters:
Implemented as links with View parameters (instead as having a backing bean with state).

Updates to the page that depend on the view params can be done with the FacesUtil.registerAfterValuesUpdate method, or with a valueChanged listener on the view param.
