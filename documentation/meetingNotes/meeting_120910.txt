
Note from meeting
=================

Survey:
* Start circulating
* Separate surveys for staff/students
  - Emphasise interface
  - Keep questions open

Foundation design
* Database
  - Hibernate
    x Does it create tables?
  - Physical DB not relevant (Hibernate supports all)

* Web tech
  - J2EE/Glassfish
  - REST?
  - Service? Servlet?
  - HTML5/CSS3 as much as possible


Basic use cases:
* Create account
  - Security
  - Provide creds, store, associate

* Log in/out
  - Give creds, get session
  - Session timeout

* Create group

* Join/Leave group

* Upload/download/submit files
  - Only submit thru groups
  - Filters (size, filename)
  - Two stage submit?
    x Suggestion: File upload screen, chosen files are not stored in the system.

* Misc features:
  - Possible to add info associated to each course, assignment, etd.
  - Integrated compile and run test on code.
  - Integrated editors
  - Unpacking of archives



