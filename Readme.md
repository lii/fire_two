The Fire 2 project
==================
 
Fire 2 is a web application for managing assignment submissions for education administrators. 

The main project directory is the following:

`./FireTwoDotZero/`

Documentation can be found in the following file:

`Fire_2_docs.pdf`

The rest of the directories contain working material that has no relevance to the finished project.


Features
--------

* Use account management: 
  - E-mail validation
  - Secure login
  - Password recovery
* Submission and re-submission of files for assignments. File upload and download.
* Bulk file download in zipped files.
* Comments system for submissions on 
* User group management, users can join and leave groups.

Tools and frameworks
--------------------

The system is built using Java EE and JavaServer Faces. 

* PrimeFaces is used to provide a richer UI with less work. 
* Apache Shiro is used to provide session management and security.  
* EJB is used for transaction management.
* CDI is used for dependency injection.
* JPA is used for object-relational mapping.
* JPQL is used for database queries. 
* Maven is used to manage building and dependencies.


